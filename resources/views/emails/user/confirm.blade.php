<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<title></title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<style>
		@import url('https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap');

		*{
			margin: 0;
			padding: 0;
			list-style-type: none;
			box-sizing: border-box;
		}

		.wrapper{
			width: 100%;
			height: auto;
			padding-bottom: 50px;
			background-color: #fafafa;
			border-top: 4px solid rgb(47, 47, 47);
		}

		.header{
			width: 100%;
			height: 106px;
			display: block;
			padding: 28px 0;
		}

		.header-logo{
			width: 50px;
			height: 50px;
			display: block;
			margin: 0 auto;
			border-radius: 5px;
		}

		.header-logo-link{
			width: 50px;
			height: 50px;
			display: block;
			border-radius: 5px;
		}

		.header-logo-img{
			width: 50px;
			height: 50px;
			display: block;
			border-radius: 5px;
		}

		.container-message{
			width: 600px;
			margin: 0 auto;
			border: 1px solid #dedede;
			background-color: #fff;
			padding: 25px 30px;
			border-radius: 2px;
		}

		.container-message-welcome{
			font-size: 16px;
			font-weight: 600;
		}

		.container-message > div{
			text-align: center;
			padding: 10px 0;
			font-size: 14px;
		}

		.container-message-confirm-link{
			text-decoration: none;
		}
	</style>
</head>
<body>
	<div class="wrapper">
		<div class="header">
			<div class="header-logo">
				<a href="{{ env('APP_FRONT_URL') }}" class="header-logo-link">
					<img src="{{ asset('frontend/images/favicon.png') }}" class="header-logo-img">
				</a>
			</div>
		</div>
		<div class="container-message">
			<div class="container-message-welcome">Подтвердите адрес электронной почты в Bilimger</div>
			<div>Здравствуйте, {{ $user->surname }} {{ $user->name }}</div>
			<div>Ваши данные для входа</div>
			<div style="width: 500px; height: 80px; margin: 10px 25px 10px 25px; border: 1px solid #dedede; border-raduis: 2px; padding: 10px;">
				<div style="width: 478px; height: 30px;">
					<div style="width: 150px; height: 30px; line-height: 30px; float: left; text-align: left; padding: 0 10px;">Имя пользователя:</div>
					<div style="width: 328px; height: 30px; line-height: 30px; float: left; text-align: left; padding: 0 10px;">{{ $user->username }}</div>
				</div>
				<div style="width: 478px; height: 30px;">
					<div style="width: 150px; height: 30px; line-height: 30px; float: left; text-align: left; padding: 0 10px;">Пароль:</div>
					<div style="width: 328px; height: 30px; line-height: 30px; float: left; text-align: left; padding: 0 10px;">{{ sha1($user->email) }}</div>
				</div>
			</div>
			<div style="padding: 10px 25px 10px 25px;">Подтвердите адрес электронной почты, чтобы активировать и защитить свой аккаунт.</div>
			<div class="container-message-confirm">
				<a href="{{ env('APP_FRONT_URL') }}/confirm?hash={{ $hash }}" class="container-message-confirm-link">Подтвердить</a>
			</div>
		</div>
	</div>
</body>
</html>