<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<title></title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href="https://fonts.googleapis.com/css2?family=Rubik:wght@300;400;500;600;700&display=swap" rel="stylesheet">

	<style>
		@import url('https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap');

		*{
			margin: 0;
			padding: 0;
			list-style-type: none;
			box-sizing: border-box;
		}

		.wrapper{
			min-width: 620px;
			width: 100%;
			height: 500px;
			padding: 0 10px;
			background-color: #fafafa;
			border-top: 4px solid rgb(47, 47, 47);
		}

		.header{
			width: 100%;
			height: 106px;
			display: block;
			padding: 28px 0;
		}

		.header-logo{
			width: 50px;
			height: 50px;
			display: block;
			margin: 0 auto;
			border-radius: 5px;
		}

		.header-logo-link{
			width: 50px;
			height: 50px;
			display: block;
			border-radius: 5px;
		}

		.header-logo-img{
			width: 50px;
			height: 50px;
			display: block;
			border-radius: 5px;
		}

		.container-message{
			width: 600px;
			margin: 0 auto;
			border: 1px solid #dedede;
			background-color: #fff;
			padding: 25px 30px;
			border-radius: 2px;
		}

		.container-message div{
			text-align: center;
			padding: 10px 0;
			font-size: 14px;
		}

		.container-message-reset-link{
			text-decoration: none;
		}
	</style>
</head>
<body>
	<div class="wrapper">
		<div class="header">
			<div class="header-logo">
				<a href="{{ env('APP_FRONT_URL') }}" class="header-logo-link">
					<img src="{{ asset('frontend/images/favicon.png') }}" class="header-logo-img">
				</a>
			</div>
		</div>
		<div class="container-message">
			<div>Здравствуйте, {{ Str::ucfirst($user->surname) }} {{ Str::ucfirst($user->name) }}</div>
			<div>Надеемся, вы, попросили сбросить пароль для вашей учетной записи Bilimger на <a href="{{ env('APP_FRONT_URL') }}">{{ env('APP_FRONT_URL') }}</a></div>
			<div>Если вы не выполнили этот запрос, можете проигнорировать это письмо.</div>
			<div>В противном случае щелкните ссылку ниже, чтобы завершить процесс.</div>
			<div class="container-message-reset">
				<a href="{{ env('APP_FRONT_URL') }}/reset?hash={{ $hash }}" class="container-message-reset-link">{{ __('messages.restore password') }}</a>
			</div>
		</div>
	</div>
</body>
</html>