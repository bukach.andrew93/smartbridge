<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\UserLinkTypeLang;
use App\Models\UserLinkType;

class UserLinkTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_link_types = [
            [
                'main' => [
                    'id' => 101
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Әкесі'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Отец'
                    ],[
                        'lang' => 'en',
                        'name' => 'Father'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 102
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Анасы'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Мать'
                    ],[
                        'lang' => 'en',
                        'name' => 'Mother'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 103
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Ұлы'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Сын'
                    ],[
                        'lang' => 'en',
                        'name' => 'Son'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 104
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Қызы'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Дочь'
                    ],[
                        'lang' => 'en',
                        'name' => 'Daughter'
                    ]
                ]
            ]
        ];

        for ($i = 0; $i < count($user_link_types); $i++) { 
            $user_link_type = UserLinkType::query()
                ->where('id', $user_link_types[$i]['main']['id'])
                ->first();

            if ($user_link_type) {
				for ($j = 0; $j < count($user_link_types[$i]['translations']); $j++) { 
                    $user_link_type_lang = UserLinkTypeLang::query()
                        ->where('id', $user_link_types[$i]['main']['id'])
                        ->where('lang', $user_link_types[$i]['translations'][$j]['lang'])
                        ->first();

                    $user_link_type_lang->update([
                        'name' => $user_link_types[$i]['translations'][$j]['name']
                    ]);
                }
            } else {
                $user_link_type = UserLinkType::create($user_link_types[$i]['main']);

                for ($j = 0; $j < count($user_link_types[$i]['translations']); $j++) { 
                    UserLinkTypeLang::create([
                        'id' => $user_link_types[$i]['main']['id'],
                        'lang' => $user_link_types[$i]['translations'][$j]['lang'],
                        'name' => $user_link_types[$i]['translations'][$j]['name']
                    ]);
                }
			}
        }
    }
}
