<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\MaritalStatus;
use App\Models\Nationality;
use App\Models\UserLang;
use App\Models\Country;
use App\Models\Gender;
use App\Models\User;
use App\Models\Role;
use App\Models\Cato;

class UserSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$users = [
			[
				'main' => [
					'id' => 100000001,
					'surname' => 'Баубек',
					'name' => 'Мақсат',
					'patronymic' => 'Асылбекұлы',
					'surname_en' => 'Baubek',
					'name_en' => 'Maksat',
					'username' => 'baubek.maksat',
					'password' => bcrypt('Farabi2020'),
					'email' => 'baubek.maksat@yandex.ru',
					'phone' => '77084184550',
					'birth_date' => '1997-02-01',
					'birth_country_id' => 316,
					'birth_cato_id' => 100000016,
					'registration_cato_id' => 100002104,
					'living_cato_id' => 100000054,
					'default_role_id' => 101,
					'default_lang_id' => 102,
					'nationality_id' => 101,
					'marital_status_id' => 1,
					'gender_id' => 1,
					'email_verified' => '1',
					'access' => '1'
				],
				'roles' => [
					[
						'slug' => 'system-administrator'
					]
				],
				'translations' => [
					[
						'lang' => 'kz',
						'surname' => 'Баубек',
						'name' => 'Мақсат',
						'patronymic' => 'Асылбекұлы',
						'registration_address' => 'Переулок Шокай Датқа, үй 3',
						'living_address' => 'Бауыржан Момышұлы д., 13а үй., 16 пәтер'
					],[
						'lang' => 'ru',
						'surname' => 'Баубек',
						'name' => 'Максат',
						'patronymic' => 'Асылбекулы',
						'registration_address' => 'Переулок Шокай Датка, д. 3',
						'living_address' => 'п. Бауыржан Момышулы, д. 13а, кв. 16',
					],[
						'lang' => 'en',
						'surname' => 'Baubek',
						'name' => 'Maksat',
						'patronymic' => 'Asylbekuly',
					]
				]
			]
		];

		for ($i = 0; $i < count($users); $i++) { 
			$user = User::query()
				->where('id', $users[$i]['main']['id'])
				->first();

			if ($user) {
				$user->update($users[$i]['main']);

				if ($users[$i]['translations']) {
					for ($j = 0; $j < count($users[$i]['translations']); $j++) { 
						$user_lang = UserLang::query()
							->where('id', $users[$i]['main']['id'])
							->where('id', $users[$i]['translations'][$j]['lang'])
							->first();

						if ($user_lang) {
							$user_lang->update([
								'registration_address' => isset($users[$i]['translations'][$j]['registration_address']) ? $users[$i]['translations'][$j]['registration_address'] : null,
								'living_address' => isset($users[$i]['translations'][$j]['living_address']) ? $users[$i]['translations'][$j]['living_address'] : null
							]);
						}
					}

					for ($j = 0; $j < count($users[$i]['roles']); $j++) { 
						$role = Role::query()
							->where('slug', $users[$i]['roles'][$j]['slug'])
							->first();
						
						if (!$user->hasRole($role->slug)) {
							$user->roles()->attach($role);
						}
					}
				}
			} else {
				$user = User::create($users[$i]['main']);

				if (isset($users[$i]['translations'])) {
					for ($j = 0; $j < count($users[$i]['translations']); $j++) { 
						UserLang::create([
							'id' => $users[$i]['main']['id'],
							'lang' => $users[$i]['translations'][$j]['lang'],
							'registration_address' => isset($users[$i]['translations'][$j]['registration_address']) ? $users[$i]['translations'][$j]['registration_address'] : null,
							'living_address' => isset($users[$i]['translations'][$j]['living_address']) ? $users[$i]['translations'][$j]['living_address'] : null
						]);
					}

					for ($j = 0; $j < count($users[$i]['roles']); $j++) { 
						$role = Role::query()
							->where('slug', $users[$i]['roles'][$j]['slug'])
							->first();

						$user->roles()->attach($role);
					}
				}
			}
		}
	}
}
