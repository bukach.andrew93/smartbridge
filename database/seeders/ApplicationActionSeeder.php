<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\ApplicationActionLang;
use App\Models\ApplicationAction;

class ApplicationActionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $application_actions = [
			[
				'main' => [
					'id' => 101
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Тіркеу',
						'make' => 'Тіркеу',
						'process' => 'Тіркелуде',
						'success' => 'Тіркелді',
						'fail' => 'Тіркелген жоқ'
					],[
						'lang' => 'ru',
						'name' => 'Регистрация',
						'make' => 'Зарегистрировать',
						'process' => 'В регистраций',
						'success' => 'Зарегистрировано',
						'fail' => 'Не зарегистрировано'
					],[
						'lang' => 'en',
						'name' => 'Registration',
						'make' => 'Зарегистрировать',
						'process' => 'Registrations',
						'success' => 'Registered',
						'fail' => 'Not registered'
					]
				]
			],[
				'main' => [
					'id' => 102
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Келісу',
						'make' => 'Келісу',
						'process' => 'Келісілуде',
						'success' => 'Келісілді',
						'fail' => 'Келісілген жоқ'
					],[
						'lang' => 'ru',
						'name' => 'Согласование',
						'make' => 'Согласовать',
						'process' => 'В согласований',
						'success' => 'Согласовано',
						'fail' => 'Не согласовано'
					],[
						'lang' => 'en',
						'name' => 'Harmonization',
						'make' => 'To approve',
						'process' => 'Approvals',
						'success' => 'Agreed',
						'fail' => 'Not agreed'
					]
				]
			],[
				'main' => [
					'id' => 103
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Мәлімдеме',
						'make' => 'Бекітілу',
						'process' => 'Registrations',
						'success' => 'Бекітілген',
						'fail' => 'Бекітілген жоқ'
					],[
						'lang' => 'ru',
						'name' => 'Утверждение',
						'make' => 'Утвердить',
						'process' => 'В утверждений',
						'success' => 'Утвержден',
						'fail' => 'Не утвержден'
					],[
						'lang' => 'en',
						'name' => 'Approval',
						'make' => 'To approve',
						'process' => 'Assertions',
						'success' => 'Approved',
						'fail' => 'Not approved'
					]
				]
			],[
				'main' => [
					'id' => 104
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Танысу',
						'make' => 'Таныстым',
						'process' => 'Танысуда',
						'success' => 'Танысты',
						'fail' => 'Танысқан жоқ'
					],[
						'lang' => 'ru',
						'name' => 'Ознакомление',
						'make' => 'Ознакомиться',
						'process' => 'В ознакомлений',
						'success' => 'Ознакомлен',
						'fail' => 'Не ознакомлено'
					],[
						'lang' => 'en',
						'name' => 'Familiarization',
						'make' => 'Familiarize',
						'process' => 'Acquaintances',
						'success' => 'Familiarized',
						'fail' => 'Not familiar'
					]
				]
			],[
				'main' => [
					'id' => 105
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Орындау',
						'make' => 'Орындау',
						'process' => 'Орындалуда',
						'success' => 'Орындалды',
						'fail' => 'Орындалған жоқ'
					],[
						'lang' => 'ru',
						'name' => 'Исполнение',
						'make' => 'Исполнить',
						'process' => 'В исполнений',
						'success' => 'Исполнен',
						'fail' => 'Не исполнен'
					],[
						'lang' => 'en',
						'name' => 'Execution',
						'make' => 'Execute',
						'process' => 'Executions',
						'success' => 'Completed',
						'fail' => 'Not executed'
					]
				]
			],[
				'main' => [
					'id' => 106
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Қолы',
						'make' => 'Қол қою',
						'process' => 'Қол қойылуда',
						'success' => 'Қол қойылды',
						'fail' => 'Қол қойылған жоқ'
					],[
						'lang' => 'ru',
						'name' => 'Подпись',
						'make' => 'Подписать',
						'process' => 'В подписаний',
						'success' => 'Подписано',
						'fail' => 'Не подписано'
					],[
						'lang' => 'en',
						'name' => 'Signature',
						'make' => 'Sign',
						'process' => 'Signatures',
						'success' => 'Signed',
						'fail' => 'Not signed'
					]
				]
			]
		];

		for ($i = 0; $i < count($application_actions); $i++) { 
			$application_action = ApplicationAction::query()
				->where('id', $application_actions[$i]['main']['id'])
				->first();

			if ($application_action) {
				$application_action->update($application_actions[$i]['main']);

				for ($j = 0; $j < count($application_actions[$i]['translations']); $j++) { 
					if ($application_action->translations()->where('lang', $application_actions[$i]['translations'][$j]['lang'])->first()) {
						$application_action->translations()->where('lang', $application_actions[$i]['translations'][$j]['lang'])->update($application_actions[$i]['translations'][$j]);
					}
				}
			} else {
				$application_action = ApplicationAction::create($application_actions[$i]['main']);

				for ($j = 0; $j < count($application_actions[$i]['translations']); $j++) { 
					ApplicationActionLang::create([
						'id' => $application_actions[$i]['main']['id'],
						'lang' => $application_actions[$i]['translations'][$j]['lang'],
						'name' => $application_actions[$i]['translations'][$j]['name'],
						'make' => $application_actions[$i]['translations'][$j]['make'],
						'process' => $application_actions[$i]['translations'][$j]['process'],
						'success' => $application_actions[$i]['translations'][$j]['success'],
						'fail' => $application_actions[$i]['translations'][$j]['fail']
					]);
				}
			}
		}
    }
}
