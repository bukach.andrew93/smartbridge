<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\SchoolLang;
use App\Models\School;

class SchoolSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$schools = [
			[
				'main' => [
					'id' => 100000001,
					'cato_id' => 100000948
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Т.Рысқұлов атындағы №48 орта мектеп'
					],[
						'lang' => 'ru',
						'name' => 'Средняя школа №48 им.Т.Рыскулова'
					],[
						'lang' => 'en',
						'name' => 'Secondary school №48 named after T.Ryskulov'
					]
				]
			]
		];

		if (count($schools) > 0) {
			for ($i = 0; $i < count($schools); $i++) { 
				$school = School::query()
					->where('id', $schools[$i]['main']['id'])
					->first();

				if ($school) {
					for ($j = 0; $j < count($schools[$i]['translations']); $j++) { 
						$school_lang = SchoolLang::query()
							->where('id', $see_types[$i]['main']['id'])
							->where('lang', $see_types[$i]['translations'][$j]['lang'])
							->first();

						$school_lang->update([
							'name' => $see_types[$i]['translations'][$j]['name']
						]);
					}
				} else {
					$school = School::create($schools[$i]['main']);

					for ($j = 0; $j < count($schools[$i]['translations']); $j++) { 
						SchoolLang::create([
							'id' => $schools[$i]['main']['id'],
							'lang' => $schools[$i]['translations'][$j]['lang'],
							'name' => $schools[$i]['translations'][$j]['name']
						]);
					}
				}
			}
		}
	}
}
