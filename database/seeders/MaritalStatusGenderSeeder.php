<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\MaritalStatusGenderLang;
use App\Models\MaritalStatusGender;

class MaritalStatusGenderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $marital_statuses_genders = [
            [
                'main' => [
                    'id' => 101,
                    'marital_status_id' => 1,
                    'gender_id' => 1
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Үйленбеген',
                    ],[
                        'lang' => 'ru',
                        'name' => 'Не женат',
                    ],[
                        'lang' => 'en',
                        'name' => 'Unmarried'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 102,
                    'marital_status_id' => 1,
                    'gender_id' => 2
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Тұрмысқа шықпаған',
                    ],[
                        'lang' => 'ru',
                        'name' => 'Не замужем',
                    ],[
                        'lang' => 'en',
                        'name' => 'Unmarried'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 103,
                    'marital_status_id' => 2,
                    'gender_id' => 1
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Үйленген',
                    ],[
                        'lang' => 'ru',
                        'name' => 'Женат',
                    ],[
                        'lang' => 'en',
                        'name' => 'Married'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 104,
                    'marital_status_id' => 2,
                    'gender_id' => 2
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Тұрмысқа шаққан',
                    ],[
                        'lang' => 'ru',
                        'name' => 'Замужем',
                    ],[
                        'lang' => 'en',
                        'name' => 'Married'
                    ]
                ]
            ]
        ];

        if (count($marital_statuses_genders) > 0) {
            for ($i = 0; $i < count($marital_statuses_genders); $i++) { 
                $marital_status_gender = MaritalStatusGender::query()
                    ->where('id', $marital_statuses_genders[$i]['main']['id'])
                    ->first();

                if ($marital_status_gender) {
                    $marital_status_gender->update($marital_statuses_genders[$i]['main']);

                    for ($j = 0; $j < count($marital_statuses_genders[$i]['translations']); $j++) { 
                        $marital_status_gender_lang = MaritalStatusGenderLang::query()
                            ->where('id', $marital_statuses_genders[$i]['main']['id'])
                            ->where('lang', $marital_statuses_genders[$i]['translations'][$j]['lang'])
                            ->first();

                        $marital_status_gender_lang->update([
                            'name' => $marital_statuses_genders[$i]['translations'][$j]['name']
                        ]);
                    }
                } else {
					$marital_status_gender = MaritalStatusGender::create($marital_statuses_genders[$i]['main']);

                    for ($j = 0; $j < count($marital_statuses_genders[$i]['translations']); $j++) { 
                        MaritalStatusGenderLang::create([
                            'id' => $marital_statuses_genders[$i]['main']['id'],
                            'lang' => $marital_statuses_genders[$i]['translations'][$j]['lang'],
                            'name' => $marital_statuses_genders[$i]['translations'][$j]['name']
                        ]);
                    }
				}
            }
        }
    }
}
