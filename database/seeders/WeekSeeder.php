<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\WeekLang;
use App\Models\Week;

class WeekSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $weeks = [
            [
                'main' => [
                    'id' => 1,
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Дүйсенбі',
						'short_name' => 'ДС'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Понедельник',
						'short_name' => 'ПН'
                    ],[
                        'lang' => 'en',
                        'name' => 'Monday',
						'short_name' => 'MO'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 2
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Сейсенбі',
						'short_name' => 'СС'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Вторник',
						'short_name' => 'ВТ'
                    ],[
                        'lang' => 'en',
                        'name' => 'Tuesday',
						'short_name' => 'TU'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 3
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Сәрсенбі',
						'short_name' => 'СР'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Среда',
						'short_name' => 'СР'
                    ],[
                        'lang' => 'en',
                        'name' => 'Wednesday',
						'short_name' => 'WE'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 4
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Бейсенбі',
						'short_name' => 'БС'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Четверг',
						'short_name' => 'ЧТ'
                    ],[
                        'lang' => 'en',
                        'name' => 'Thursday',
						'short_name' => 'TH'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 5
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Жұма',
						'short_name' => 'ЖМ'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Пятница',
						'short_name' => 'ПТ'
                    ],[
                        'lang' => 'en',
                        'name' => 'Friday',
						'short_name' => 'FR'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 6
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Сенбі',
						'short_name' => 'СБ'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Суббота',
						'short_name' => 'СБ'
                    ],[
                        'lang' => 'en',
                        'name' => 'Saturday',
						'short_name' => 'SA'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 7
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Жексенбі',
						'short_name' => 'ЖС'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Воскресенье',
						'short_name' => 'ВС'
                    ],[
                        'lang' => 'en',
                        'name' => 'Sunday',
						'short_name' => 'SU'
                    ]
                ]
            ]
        ];

        if (count($weeks) > 0) {
            for ($i = 0; $i < count($weeks); $i++) { 
            	$week = Week::query()
                    ->where('id', $weeks[$i]['main']['id'])
                    ->first();

                if ($week) {
					for ($j = 0; $j < count($weeks[$i]['translations']); $j++) {
						$week_lang = WeekLang::query()
                            ->where('id', $weeks[$i]['main']['id'])
                            ->where('lang', $weeks[$i]['translations'][$j]['lang'])
                            ->first();

                        $week_lang->update([
                            'name' => $weeks[$i]['translations'][$j]['name'],
                            'short_name' => $weeks[$i]['translations'][$j]['short_name']
                        ]);
					}
                } else {
					$week = Week::create($weeks[$i]['main']);
					
					for ($j = 0; $j < count($weeks[$i]['translations']); $j++) {
						WeekLang::create([
                            'id' => $weeks[$i]['main']['id'],
                            'lang' => $weeks[$i]['translations'][$j]['lang'],
                            'name' => $weeks[$i]['translations'][$j]['name'],
                            'short_name' => $weeks[$i]['translations'][$j]['short_name']
                        ]);
					}
				}
            }
        }
    }
}
