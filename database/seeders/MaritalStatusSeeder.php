<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\MaritalStatusLang;
use App\Models\MaritalStatus;

class MaritalStatusSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$marital_statuses = [
			[
				'main' => [
					'id' => 1
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Үйленбеген|Тұрмысқа шықпаған'
					],[
						'lang' => 'ru',
						'name' => 'Не женат|Не замужем'
					],[
						'lang' => 'en',
						'name' => 'Unmarried'
					]
				]
			],[
				'main' => [
					'id' => 2
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Үйлеген|Тұрмысқа шаққан'
					],[
						'lang' => 'ru',
						'name' => 'Женат|Замужем'
					],[
						'lang' => 'en',
						'name' => 'Married'
					]
				]
			]
		];

		for ($i = 0; $i < count($marital_statuses); $i++) { 
			$marital_status = MaritalStatus::query()
				->where('id', $marital_statuses[$i]['main']['id'])
				->first();

			if ($marital_status) {
				$marital_status->update($marital_statuses[$i]['main']);

				for ($j = 0; $j < count($marital_statuses[$i]['translations']); $j++) { 
					$marital_status_lang = MaritalStatusLang::query()
						->where('id', $marital_statuses[$i]['main']['id'])
						->where('lang', $marital_statuses[$i]['translations'][$j]['lang'])
						->first();

					$marital_status_lang->update([
						'name' => $marital_statuses[$i]['translations'][$j]['name']
					]);
				}
			} else {
				$marital_status = MaritalStatus::create($marital_statuses[$i]['main']);

				for ($j = 0; $j < count($marital_statuses[$i]['translations']); $j++) { 
					MaritalStatusLang::create([
						'id' => $marital_statuses[$i]['main']['id'],
						'lang' => $marital_statuses[$i]['translations'][$j]['lang'],
						'name' => $marital_statuses[$i]['translations'][$j]['name']
					]);
				}
			}
		}
	}
}
