<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\ReplenishmentViewLang;
use App\Models\ReplenishmentView;

class ReplenishmentViewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $replenishment_views = [
            [
                'main' => [
                    'id' => 101,
                    'slug' => '1c-accounting',
                    'icon' => 'awdawdawd'
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => '1С Бухгалтерлік есеп'
                    ],[
                        'lang' => 'ru',
                        'name' => '1C Бухгалтерия'
                    ],[
                        'lang' => 'en',
                        'name' => '1C Accounting'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 102,
                    'slug' => 'online-banking',
                    'icon' => 'awdawdawd'
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Интернет банкинг',
                    ],[
                        'lang' => 'ru',
                        'name' => 'Онлайн банкинг'
                    ],[
                        'lang' => 'en',
                        'name' => 'Online banking'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 103,
                    'slug' => 'transfer',
                    'icon' => 'awdawdaw'
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Аударым',
                    ],[
                        'lang' => 'ru',
                        'name' => 'Перевод'
                    ],[
                        'lang' => 'en',
                        'name' => 'Transfer'
                    ]
                ]
            ]
        ];

        for ($i = 0; $i < count($replenishment_views); $i++) { 
            $replenishment_view = ReplenishmentView::query()
                ->where('id', $replenishment_views[$i]['main']['id'])
                ->first();

            if ($replenishment_view) {
                $replenishment_view->update($replenishment_views[$i]['main']);

                for ($j = 0; $j < count($replenishment_views[$i]['translations']); $j++) { 
                    $replenishment_view_lang = ReplenishmentViewLang::query()
                        ->where('id', $replenishment_views[$i]['main']['id'])
                        ->where('lang', $replenishment_views[$i]['translations'][$j]['lang'])
                        ->first();

                    $replenishment_view_lang->update([
                        'name' => $replenishment_views[$i]['translations'][$j]['name']
                    ]);
                }
            } else {
                $replenishment_view = ReplenishmentView::create($replenishment_views[$i]['main']);

                for ($j = 0; $j < count($replenishment_views[$i]['translations']); $j++) { 
                    ReplenishmentViewLang::create([
                        'id' => $replenishment_views[$i]['main']['id'],
                        'lang' => $replenishment_views[$i]['translations'][$j]['lang'],
                        'name' => $replenishment_views[$i]['translations'][$j]['name']
                    ]);
                }
            }
        }
    }
}
