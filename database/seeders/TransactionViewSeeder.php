<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\TransactionViewLang;
use App\Models\TransactionView;

class TransactionViewSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$transaction_views = [
			[  
				'main' => [
					'id' => 101,
					'slug' => 'replenishment',
					'icon' => 'frontend/images/icon-coins-FFD949.svg'
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Толтыру'
					],[
						'lang' => 'ru',
						'name' => 'Пополнение'
					],[
						'lang' => 'en',
						'name' => 'Replenishment'
					]
				]
			],[
				'main' => [
					'id' => 102,
					'slug' => 'payment',
					'icon' => 'frontend/images/icon-invoice.svg'
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Төлем'
					],[
						'lang' => 'ru',
						'name' => 'Платеж'
					],[
						'lang' => 'en',
						'name' => 'Payment'
					]
				]
			],[
				'main' => [
					'id' => 103,
					'slug' => 'transfer',
					'icon' => 'frontend/images/icon-switch.svg'
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Аударым'
					],[
						'lang' => 'ru',
						'name' => 'Перевод'
					],[
						'lang' => 'en',
						'name' => 'Transfer'
					]
				]
			],[
				'main' => [
					'id' => 104,
					'slug' => 'accrual',
					'icon' => null
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Есептеу'
					],[
						'lang' => 'ru',
						'name' => 'Начисление'
					],[
						'lang' => 'en',
						'name' => 'Accrual'
					]
				]
			],[
				'main' => [
					'id' => 105,
					'slug' => 'refund',
					'icon' => null
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Қайтару'
					],[
						'lang' => 'ru',
						'name' => 'Возврат'
					],[
						'lang' => 'en',
						'name' => 'Refund'
					]
				]
			]
		];

		for ($i = 0; $i < count($transaction_views); $i++) { 
			$transaction_view = TransactionView::query()
				->where('id', $transaction_views[$i]['main']['id'])
				->first();

			if ($transaction_view) {
				$transaction_view->update($transaction_views[$i]['main']);

				for ($j = 0; $j < count($transaction_views[$i]['translations']); $j++) { 
					$transaction_view_lang = TransactionViewLang::query()
						->where('id', $transaction_views[$i]['main']['id'])
						->where('lang', $transaction_views[$i]['translations'][$j]['lang'])
						->first();

					$transaction_view_lang->update([
						'name' => $transaction_views[$i]['translations'][$j]['name']
					]);
				}
			} else {
				$transaction_view = TransactionView::create($transaction_views[$i]['main']);

				for ($j = 0; $j < count($transaction_views[$i]['translations']); $j++) { 
					TransactionViewLang::create([
                        'id' => $transaction_views[$i]['main']['id'],
                        'lang' => $transaction_views[$i]['translations'][$j]['lang'],
                        'name' => $transaction_views[$i]['translations'][$j]['name']
                    ]);
				}
			}
		}
	}
}
