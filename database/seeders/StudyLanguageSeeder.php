<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\StudyLanguageLang;
use App\Models\StudyLanguage;

class StudyLanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$study_languages = [
            [
                'main' => [
                    'id' => 101
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Қазақ'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Казахский'
                    ],[
                        'lang' => 'en',
                        'name' => 'Kazakh'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 102
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Орыс'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Русский'
                    ],[
                        'lang' => 'en',
                        'name' => 'Russian'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 103
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Ағылшын'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Английский'
                    ],[
                        'lang' => 'en',
                        'name' => 'English'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 104
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Қытайша'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Китайский'
                    ],[
                        'lang' => 'en',
                        'name' => 'Chinese'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 105
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Немісше'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Немецкий'
                    ],[
                        'lang' => 'en',
                        'name' => 'German'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 106
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Француз'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Французский'
                    ],[
                        'lang' => 'en',
                        'name' => 'French'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 107
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Испан'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Испанский'
                    ],[
                        'lang' => 'en',
                        'name' => 'Spanish'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 108
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Корей'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Корейский'
                    ],[
                        'lang' => 'en',
                        'name' => 'Korean'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 109
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Италиян'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Итальянский'
                    ],[
                        'lang' => 'en',
                        'name' => 'Italian'
                    ]
                ]
            ],[
                'main' => [
                    'id' => 110
                ],
                'translations' => [
                    [
                        'lang' => 'kz',
                        'name' => 'Араб'
                    ],[
                        'lang' => 'ru',
                        'name' => 'Арабский'
                    ],[
                        'lang' => 'en',
                        'name' => 'Arabic'
                    ]
                ]
            ]
        ];

        for ($i = 0; $i < count($study_languages); $i++) { 
            $study_language = StudyLanguage::query()
                ->where('id', $study_languages[$i]['main']['id'])
                ->first();

            if ($study_language) {
                $study_language->update($study_languages[$i]['main']);

                for ($j = 0; $j < count($study_languages[$i]['translations']); $j++) { 
                    $study_language = StudyLanguageLang::query()
                        ->where('id', $study_languages[$i]['main']['id'])
                        ->where('lang', $study_languages[$i]['translations'][$j]['lang'])
                        ->first();

                    $study_language->update([
                        'name' => $study_languages[$i]['translations'][$j]['name']
                    ]);
                }
            } else {
                $study_language = StudyLanguage::create($study_languages[$i]['main']);
				
				for ($j = 0; $j < count($study_languages[$i]['translations']); $j++) { 
                    StudyLanguageLang::create([
                        'id' => $study_languages[$i]['main']['id'],
                        'lang' => $study_languages[$i]['translations'][$j]['lang'],
                        'name' => $study_languages[$i]['translations'][$j]['name']
                    ]);
                }
            }
        }
    }
}
