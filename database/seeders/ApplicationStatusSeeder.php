<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\ApplicationStatusLang;
use App\Models\ApplicationStatus;

class ApplicationStatusSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$application_statuses = [
			[
				'main' => [
					'id' => 1,
					'slug' => 'waiting'
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Күту'
					],[
						'lang' => 'ru',
						'name' => 'В ожиданий'
					],[
						'lang' => 'en',
						'name' => 'Expectation'
					]
				]
			],[
				'main' => [
					'id' => 2,
					'slug' => 'processing'
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Өңдеуде'
					],[
						'lang' => 'ru',
						'name' => 'В обработке'
					],[
						'lang' => 'en',
						'name' => 'Processing'
					]
				]
			],[
				'main' => [
					'id' => 3,
					'slug' => 'ready'
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Бекітілген'
					],[
						'lang' => 'ru',
						'name' => 'Одобрено'
					],[
						'lang' => 'en',
						'name' => 'Ready'
					]
				]
			],[
				'main' => [
					'id' => 4,
					'slug' => 'renouncement'
				],
				'translations' => [
					[
						'lang' => 'kz',
						'name' => 'Қабылданбады'
					],[
						'lang' => 'ru',
						'name' => 'Отказано'
					],[
						'lang' => 'en',
						'name' => 'Denied'
					]
				]
			]
		];

		for ($i = 0; $i < count($application_statuses); $i++) { 
			$application_status = ApplicationStatus::query()
				->where('id', $application_statuses[$i]['main']['id'])
				->first();

			if ($application_status) {
				$application_status->update($application_statuses[$i]['main']);

				for ($j = 0; $j < count($application_statuses[$i]['translations']); $j++) { 
					$application_status_lang = ApplicationStatusLang::query()
						->where('id', $application_statuses[$i]['main']['id'])
						->where('lang', $application_statuses[$i]['translations'][$j]['lang'])
						->first();

					$application_status_lang->update([
						'name' => $application_statuses[$i]['translations'][$j]['name']
					]);
				}
			} else {
				$application_status = ApplicationStatus::create($application_statuses[$i]['main']);

				for ($j = 0; $j < count($application_statuses[$i]['translations']); $j++) { 
					ApplicationStatusLang::create([
                        'id' => $application_statuses[$i]['main']['id'],
                        'lang' => $application_statuses[$i]['translations'][$j]['lang'],
                        'name' => $application_statuses[$i]['translations'][$j]['name']
                    ]);
				}
			}
		}
	}
}
