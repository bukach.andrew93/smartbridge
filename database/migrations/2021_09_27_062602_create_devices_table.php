<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDevicesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('devices', function (Blueprint $table) {
			$table->id();
			$table->unsignedBigInteger('platform_id');
			$table->string('brand');
			$table->string('model');
			$table->string('imei');
			$table->string('width');
			$table->string('height');
			$table->string('signature', 256);
			$table->string('ip');
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();

            $table->foreign('platform_id')->references('id')->on('platforms');
		});

        DB::statement('ALTER TABLE devices AUTO_INCREMENT = 100000001');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('devices');
	}
}
