<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationViewLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_view_langs', function (Blueprint $table) {
            $table->unsignedBigInteger('id');
            $table->string('lang');
            $table->string('name');
            $table->text('instruction')->nullable();
            $table->json('template')->nullable();
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('id')->references('id')->on('application_views');
            $table->foreign('lang')->references('slug')->on('langs');

            $table->primary([
                'id',
                'lang'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_view_langs');
    }
}
