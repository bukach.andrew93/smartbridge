<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClassroomsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('classrooms', function (Blueprint $table) {
			$table->id();
			$table->unsignedBigInteger('campus_id');
			$table->unsignedBigInteger('type_id');
			$table->unsignedBigInteger('capacity');
			$table->unsignedBigInteger('state_id');
			$table->json('integration_fields')->nullable();
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();

			$table->foreign('campus_id')->references('id')->on('campuses');
			$table->foreign('type_id')->references('id')->on('classroom_types');
			$table->foreign('state_id')->references('id')->on('states');
		});

		DB::statement('ALTER TABLE classrooms AUTO_INCREMENT = 100001');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('classrooms');
	}
}
