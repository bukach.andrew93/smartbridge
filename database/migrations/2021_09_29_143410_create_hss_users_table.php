<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHssUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hss_users', function (Blueprint $table) {
            $table->unsignedBigInteger('hs_id');
            $table->unsignedBigInteger('user_id');
            $table->enum('vote', ['per', 'vs']);
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('hs_id')->references('id')->on('hss');
            $table->foreign('user_id')->references('id')->on('users');

            $table->primary([
                'hs_id',
                'user_id'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hss_users');
    }
}
