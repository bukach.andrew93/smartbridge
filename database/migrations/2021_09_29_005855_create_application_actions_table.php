<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationActionsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('application_actions', function (Blueprint $table) {
			$table->id();
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();
		});

		DB::statement('ALTER TABLE application_actions AUTO_INCREMENT = 101');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('application_actions');
	}
}
