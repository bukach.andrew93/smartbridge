<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTranscriptLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transcript_langs', function (Blueprint $table) {
            $table->unsignedBigInteger('id');
            $table->string('lang');
            $table->string('subject_code')->nullable();
            $table->string('subject_name')->nullable();
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('id')->references('id')->on('transcripts');
            $table->foreign('lang')->references('slug')->on('langs');

            $table->primary([
                'id',
                'lang'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transcript_langs');
    }
}
