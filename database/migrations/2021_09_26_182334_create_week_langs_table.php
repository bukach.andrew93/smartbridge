<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeekLangsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('week_langs', function (Blueprint $table) {
			$table->unsignedBigInteger('id');
			$table->string('lang');
			$table->string('name')->unique();
			$table->string('short_name');
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();

			$table->foreign('id')->references('id')->on('weeks');
			$table->foreign('lang')->references('slug')->on('langs');

			$table->primary([
				'id',
				'lang'
			]);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('week_langs');
	}
}
