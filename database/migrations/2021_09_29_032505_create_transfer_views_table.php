<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransferViewsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transfer_views', function (Blueprint $table) {
			$table->id();
			$table->string('slug')->unique();
			$table->string('icon');
			$table->timestamps();
			$table->timestamp('archived_at')->nullable();
			$table->softDeletes();
		});

		DB::statement('ALTER TABLE transfer_views AUTO_INCREMENT = 101');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('transfer_views');
	}
}
