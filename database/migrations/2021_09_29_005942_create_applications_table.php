<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('view_id');
            $table->unsignedBigInteger('role_id');
            $table->unsignedBigInteger('responsible_role_id')->nullable();
            $table->unsignedBigInteger('responsible_user_id')->nullable();
            $table->unsignedBigInteger('status_id');
            $table->string('lang');
            $table->text('src')->nullable();
            $table->text('message')->nullable();
            $table->json('files')->nullable();
            $table->text('hash')->nullable();
            $table->text('qr')->nullable();
            $table->text('signature')->nullable();
            $table->timestamp('delivered_at')->nullable();
            $table->timestamp('started_at')->nullable();
            $table->timestamp('ended_at')->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('view_id')->references('id')->on('application_views');
            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('responsible_user_id')->references('id')->on('users');
            $table->foreign('status_id')->references('id')->on('application_statuses');
            $table->foreign('lang')->references('slug')->on('langs');
        });

        DB::statement('ALTER TABLE applications AUTO_INCREMENT = 100000001');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
