<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIntegrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('integrations', function (Blueprint $table) {
            $table->id();
            $table->string('system');
            $table->string('table');
            $table->string('batch');
            $table->unsignedBigInteger('created');
            $table->unsignedBigInteger('updated');
            $table->unsignedBigInteger('deleted');
            $table->string('time');
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();
        });

        DB::statement('ALTER TABLE integrations AUTO_INCREMENT = 100000000001');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('integrations');
    }
}
