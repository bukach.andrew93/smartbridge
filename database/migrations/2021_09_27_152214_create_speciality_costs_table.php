<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecialityCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('speciality_costs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('speciality_id');
            $table->unsignedBigInteger('study_form_id');
            $table->unsignedBigInteger('state_id');
            $table->year('year_of_admission');
            $table->year('study_year');
            $table->unsignedBigInteger('value');
            $table->json('integration_fields')->nullable();
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('speciality_id')->references('id')->on('specialities');
            $table->foreign('study_form_id')->references('id')->on('study_forms');
            $table->foreign('state_id')->references('id')->on('states');
        });

        DB::statement('ALTER TABLE speciality_costs AUTO_INCREMENT = 100000001');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('speciality_costs');
    }
}
