<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcademicCalendarTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academic_calendar_types', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('terms_count');
            $table->unsignedBigInteger('term_duration');
            $table->unsignedBigInteger('state_id');
            $table->json('integration_fields')->nullable();
            $table->timestamps();
            $table->timestamp('archived_at')->nullable();
            $table->softDeletes();

            $table->foreign('state_id')->references('id')->on('states');
        });

        DB::statement('ALTER TABLE academic_calendar_types AUTO_INCREMENT = 101');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('academic_calendar_types');
    }
}
