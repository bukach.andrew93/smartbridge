<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class StudentServiceCenterOperatingMode extends Model
{
    use HasFactory, Observer;

    protected $fillable = [
    	'id',
    	'week_id',
    	'time_start',
    	'time_end'
    ];
	
	public function week()
	{
		return $this->hasOne(Week::class, 'id', 'week_id');
	} 
}
