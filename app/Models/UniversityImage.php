<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class UniversityImage extends Model
{
    use HasFactory, Observer;

    protected $table = 'university_images';

    protected $fillable = [
    	'id',
    	'src'
    ];
}
