<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Observer;

class Hs extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'hss';

    protected $fillable = [
    	'id',
    	'state_id',
		'category_id',
        'per',
        'vs'
    ];

    public function translation()
    {
        return $this->hasOne(HsLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(HsLang::class, 'id', 'id');
    }

    public function state()
    {
        return $this->hasOne(State::class, 'id', 'state_id');
    }

    public function category()
    {
        return $this->hasOne(HsCategory::class, 'id', 'category_id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'hss_users');
    }

    public function hasUser($id) {
        if ($this->users->contains('id', $id)) {
            return true;
        } else {
            return false;
        }
    }
    
    public function getQuestionAttribute()
    {
        return $this->translation->question;
    }
    
    public function getAnswerAttribute()
    {
        return $this->translation->answer;
    }
}
