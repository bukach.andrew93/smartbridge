<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class UserLang extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'user_langs';

    protected $fillable = [
        'id', 
        'lang', 
        'surname',  
        'name', 
        'patronymic', 
        'birth_cato_name',
        'registration_address',
        'living_address'
    ];
}
