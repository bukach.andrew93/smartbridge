<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Platform extends Model
{
	use HasFactory, Observer;

	protected $table = 'platforms';

	protected $fillable = [
		'id',
		'type_id',
		'slug'
	];

	public function getRouteKeyName()
	{
		return 'id';
	}

	public function translation()
	{
		return $this->hasOne(PlatformLang::class, 'id', 'id')
			->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
				? auth()->user()->defaultLangSlug()
				: app()->getLocale()
			);
	}

	public function translations()
	{
		return $this->hasMany(PlatformLang::class, 'id', 'id');
	}

	public function type()
	{
		return $this->hasOne(Type::class, 'id', 'type_id');
	}
	
	public function getNameAttribute()
	{
		return $this->translation->name;
	}
}
