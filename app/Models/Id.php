<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Id extends Model
{
    use HasFactory, Observer;

    protected $table = 'ids';

    protected $fillable = [
        'id',
        'user_id', 
        'type_id', 
        'citizenship_id',
        'issuing_authority_id',
        'date_start', 
        'date_end',
        'series',
        'number',
        'tin',
        'src'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function type()
    {
        return $this->hasOne(IdType::class, 'id', 'type_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function citizenship()
    {
        return $this->hasOne(Country::class, 'id', 'citizenship_id');
    }

    public function issuingAuthority()
    {
        return $this->hasOne(IdIssuingAuthority::class, 'id', 'issuing_authority_id');
    }
}
