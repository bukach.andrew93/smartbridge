<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class AcademicCalendarType extends Model
{
    use HasFactory, Observer;

    protected $table = 'academic_calendar_types';

    protected $fillable = [
    	'id',
        'platonus_key',
        'university_id',
        'terms_count',
        'term_duration',
        'state_id'
    ];

    public function translation()
    {
        return $this->hasOne(AcademicCalendarTypeLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(AcademicCalendarTypeLang::class, 'id', 'id');
    }
}
