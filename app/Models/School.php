<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class School extends Model
{
    use HasFactory, Observer;

    protected $fillable = [
    	'id', 
    	'cato_id'
    ];

    public function translation()
    {
        return $this->hasOne(SchoolLang::class, 'id', 'id')->where('lang', app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(SchoolLang::class, 'id', 'id');
    }
}
