<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class StudentServiceCenterRoleLang extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'student_service_center_role_langs';

    protected $fillable = [
    	'id',
        'lang',
        'name'
    ];
	
	public function snr()
	{
		return $this->hasOne(StudentServiceCenterRole::class, 'id', 'id');
	}
}
