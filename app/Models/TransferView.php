<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class TransferView extends Model
{
    use HasFactory, Observer;

    protected $table = 'transfer_views';

    protected $fillable = [
    	'id',
        'slug'
    ];

    public function translation()
    {
        return $this->hasOne(TransferViewLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(TransferViewLang::class, 'id', 'id');
    }
    
    public function getNameAttribute()
    {
        return $this->translation->name;
    }
}
