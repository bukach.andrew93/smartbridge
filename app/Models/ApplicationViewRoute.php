<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class StatementViewRoute extends Model
{
    use HasFactory, Observer;

    protected $table = 'statement_view_routes';

    protected $fillable = [
    	'id',
    	'stage_id',
    	'action_id',
		'responsible_type',
		'responsible_role_id',
    	'responsible_user_id',
        'term'
    ];

    public function action()
    {
        return $this->hasOne(StatementAction::class, 'id', 'action_id');
    }

    public function role()
    {
        return $this->hasOne(StudentServiceCenterRole::class, 'id', 'responsible_role_id')->withTrashed();
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'responsible_user_id')->withTrashed();
    }
}
