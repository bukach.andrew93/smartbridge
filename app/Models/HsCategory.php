<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Observer;

class HsCategory extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'hs_categories';

    protected $fillable = [
    	'id',
		'university_id',
		'state_id',
    	'icon'
    ];

    public function translation()
    {
        return $this->hasOne(HsCategoryLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(HsCategoryLang::class, 'id', 'id');
    }
	
	public function getNameAttribute()
    {
        return $this->translation->name;
    }

    public function state()
    {
        return $this->hasOne(State::class, 'id', 'state_id');
    }

    public function hss()
    {
        return $this->hasMany(Hs::class, 'category_id', 'id');
    }
}
