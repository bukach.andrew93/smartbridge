<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class WorkerPosition extends Model
{
    use HasFactory, Observer;

    protected $table = 'worker_positions';

    protected $fillable = [
    	'id'
    ];

    public function translation()
    {
        return $this->hasOne(WorkerPositionLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(WorkerPositionLang::class, 'id', 'id');
    }
	
	public function studentServiceCenter()
	{
        return $this->hasOne(StudentServiceCenterUser::class, 'worker_position_id', 'id');
    }

    public function getNameAttribute()
    {
        return $this->translation->name;
    }
}
