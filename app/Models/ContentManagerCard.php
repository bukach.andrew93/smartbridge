<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class ContentManagerCard extends Model
{
    use HasFactory, Observer;

    protected $table = 'content_manager_cards';

    protected $fillable = [
        'id',
        'university_id',
        'user_id',
        'date_start',
        'date_end'
    ];

    protected $casts = [
        'date_start' => 'date',
        'date_end' => 'date'
    ];

    public function university()
    {
        return $this->hasOne(University::class, 'id', 'university_id');
    }
}
