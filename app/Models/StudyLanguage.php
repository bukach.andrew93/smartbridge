<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class StudyLanguage extends Model
{
    use HasFactory, Observer;

    protected $fillable = [
    	'id',
        'platonus_key'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function translation()
    {
        return $this->hasOne(StudyLanguageLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(StudyLanguageLang::class, 'id', 'id');
    }
	
	public function universities()
	{
		return $this->belongsToMany(University::class, 'universities_study_languages', 'study_language_id', 'university_id');
	}
	
	public function getNameAttribute()
    {
        return $this->translation->name;
    }
}
