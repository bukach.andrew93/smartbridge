<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class UniversityEmail extends Model
{
    use HasFactory, Observer;

    protected $table = 'university_emails';

    protected $fillable = [
    	'id',
    	'value'
    ];
}
