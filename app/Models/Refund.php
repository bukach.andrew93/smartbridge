<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Refund extends Model
{
	use HasFactory, Observer;

	protected $fillable = [
		'id',
		'reference_id'
	];

	public function reference()
	{
		return $this->hasOne(Transaction::class, 'id', 'reference_id');
	}
}
