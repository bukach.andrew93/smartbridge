<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class AppRelease extends Model
{
    use HasFactory, Observer;

    protected $table = 'app_releases';

    protected $fillable = [
    	'id',
        'university_id',
        'platform_id',
        'state_id',
        'version',
        'name',
        'link'
    ];

    public function university()
    {
        return $this->hasOne(University::class, 'id', 'university_id');
    }

    public function platform()
    {
        return $this->hasOne(Platform::class, 'id', 'platform_id');
    }

    public function state()
    {
        return $this->hasOne(State::class, 'id', 'state_id');
    }
}
