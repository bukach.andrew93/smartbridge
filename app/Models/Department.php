<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Department extends Model
{
    use HasFactory, Observer;

    protected $table = 'departments';

    protected $fillable = [
        'id',
        'platonus_key',
    	'faculty_id',
    	'administrator_id',
        'state_id'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function translation()
    {
        return $this->hasOne(DepartmentLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(DepartmentLang::class, 'id', 'id');
    }

    public function faculty()
    {
        return $this->hasOne(Faculty::class, 'id', 'faculty_id');
    }

    public function state()
    {
        return $this->hasOne(State::class, 'id', 'state_id');
    }

    public function administrator()
    {
        return $this->hasOne(User::class, 'id', 'administrator_id');
    }
	
	public function getNameAttribute()
	{
		//	return $this->translation->name;
		return $this->translation ? $this->translation->name : null;
	}
}
