<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Country extends Model
{
    use HasFactory, Observer;

    protected $table = 'countries';

    protected $fillable = [
        'vmp_id',
    	'flag',
    	'code',
        'state_id'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function documentTypes()
    {
        return $this->belongsToMany(UserDocumentType::class, 'user_document_types_citizenships', 'citizenship_id', 'type_id');
    }

    public function catos()
    {
        return $this->hasMany(Cato::class, 'country_id', 'id')->with('catos');
    }

    public function translation()
    {
        return $this->hasOne(CountryLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLang
                ? auth()->user()->defaultLang->slug
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(CountryLang::class, 'id', 'id');
    }
	
	public function getNameAttribute()
	{
		return $this->translation ? $this->translation->name : null;
	}
}
