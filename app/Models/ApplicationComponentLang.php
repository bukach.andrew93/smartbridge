<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class StatementComponentLang extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'statement_component_langs';

    protected $fillable = [
    	'id',
    	'lang', 
    	'name',
        'data'
    ];

    protected $casts = [
    	'data' => 'array'
    ];
}
