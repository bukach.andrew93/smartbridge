<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Month extends Model
{
    use HasFactory, Observer;

    protected $table = 'months';

    protected $fillable = [
    	'id'
    ];

    public function translation()
    {
        return $this->hasOne(AcademicCalendarLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(AcademicCalendarLang::class, 'id', 'id');
    }
	
	public function getFirstNameAttribute()
	{
        return $this->translation->name;
    }
}
