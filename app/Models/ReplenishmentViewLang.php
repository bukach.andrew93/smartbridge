<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class ReplenishmentViewLang extends Model
{
    use HasFactory, Observer;

    public $incrementing = false;

    protected $table = 'replenishment_view_langs';

    protected $fillable = [
    	'id',
    	'lang',
    	'name'
    ];
}
