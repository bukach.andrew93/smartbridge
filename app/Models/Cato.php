<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Cato extends Model
{
    use HasFactory, Observer;

    protected $table = 'catos';

    protected $fillable = [
    	'id',
        'country_id',
        'parent_id',
        'code',
    	'level'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function country()
    {
        return $this->hasOne(Country::class, 'id', 'country_id');
    }

    public function cato()
    {
        return $this->hasOne(Cato::class, 'id', 'parent_id');
    }

    public function parent()
    {
        return $this->hasOne(Cato::class, 'id', 'parent_id')->with('cato');
    }

    public function catos()
    {
        return $this->hasMany(Cato::class, 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany(Cato::class, 'parent_id', 'id')->with('catos');
    }

    public function translation()
    {
        return $this->hasOne(CatoLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(CatoLang::class, 'id', 'id');
    }
	
	public function getNameAttribute()
    {
        return $this->translation ? $this->translation->name : null;
    }
}
