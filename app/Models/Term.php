<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Term extends Model
{
    use HasFactory, Observer;

    protected $table = 'terms';

    protected $fillable = [
    	'id',
		'university_id',
		'platonus_key',
		'type_id',
		'academic_calendar_id',
        'state_id',
		'date_start',
		'date_end'
    ];

    public function university()
    {
        return $this->hasOne(University::class, 'id', 'university_id');
    }

    public function translation()
    {
        return $this->hasOne(TermLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(TermLang::class, 'id', 'id');
    }
}
