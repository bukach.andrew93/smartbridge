<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class Lang extends Model
{
    use HasFactory, Observer;

    protected $fillable = [
    	'id', 
    	'slug', 
    	'name'
    ];

    public function getBySlug($slug)
    {
        return $this->where('slug', $slug);
    }
}
