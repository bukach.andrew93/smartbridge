<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class AcademicCalendar extends Model
{
    use HasFactory, Observer;

    protected $table = 'academic_calendars';

    protected $fillable = [
    	'id',
        'platonus_key',
        'type_id',
        'study_form_id',
        'all_specialities',
        'state_id'
    ];

    public function studyForm()
    {
        return $this->hasOne(StudyForm::class, 'id', 'study_form_id');
    }

    public function translation()
    {
        return $this->hasOne(AcademicCalendarLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(AcademicCalendarLang::class, 'id', 'id');
    }
}
