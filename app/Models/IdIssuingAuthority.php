<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class IdIssuingAuthority extends Model
{
    use HasFactory, Observer;

    protected $table = 'id_issuing_authorities';

    protected $fillable = [
    	'id',
        'platonus_key',
        'country_id'
    ];

    public function translation()
    {
        return $this->hasOne(IdIssuingAuthorityLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLangSlug()
                ? auth()->user()->defaultLangSlug()
                : app()->getLocale()
            );
    }

    public function translations()
    {
        return $this->hasMany(IdIssuingAuthorityLang::class, 'id', 'id');
    }

    public function citizenships()
    {
        return $this->hasMany(UserIdTypeCitizenship::class, 'type_id', 'id');
    }

    public function country()
    {
        return $this->hasOne(Citizenship::class, 'id', 'country_id');
    }
    
    public function getNameAttribute()
    {
        return $this->translation->name;
    }
}
