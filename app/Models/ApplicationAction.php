<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Traits\Observer;

class ApplicationAction extends Model
{
    use HasFactory, Observer;

    protected $table = 'application_actions';

    protected $fillable = [
    	'id'
    ];

    public function translation()
    {
        return $this->hasOne(ApplicationActionLang::class, 'id', 'id')
            ->where('lang', auth()->check() && auth()->user()->defaultLang ? auth()->user()->defaultLang->slug : app()->getLocale());
    }

    public function translations()
    {
        return $this->hasMany(ApplicationActionLang::class, 'id', 'id');
    }

    public function getNameAttribute()
    {
        return $this->translation->name;
    }

    public function getMakeAttribute()
    {
        return $this->translation->make;
    }

    public function getProcessAttribute()
    {
        return $this->translation->process;
    }

    public function getSuccessAttribute()
    {
        return $this->translation->success;
    }

    public function getFailAttribute()
    {
        return $this->translation->fail;
    }
}
