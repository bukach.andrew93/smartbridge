<?php

namespace App\Listeners;

use App\Events\StatementCommentCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

use Pusher\Pusher as Pusher;

use DB;

class SendStatementComment// implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StatementStatusProcessed  $event
     * @return void
     */
    public function handle(StatementCommentCreated $event)
    {
    	$options = [
			'cluster' => 'ap2',
			'useTLS' => true
		];

		$pusher = new Pusher(
			'3ca4cf8be0971f59e092',
			'29f892205faa2ca88608',
			'1295256',
			$options
		);

		$data['statement_comment'] = [
			'id' => $event->statement_comment->id,
			'statement' => [
				'id' => $event->statement_comment->statement->id
			],
			'message' => $event->statement_comment->message
		];

		//	$pusher->trigger('statement-s.'.$event->statement->id, 'show', $data);
		$pusher->trigger('statement-comments', 'store', $data);
    }
}
