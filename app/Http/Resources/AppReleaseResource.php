<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AppReleaseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'link' => $this->link,
            'version' => $this->version,
            'university' => $this->university ? [
                'id' => $this->university->id,
                'name' => $this->university->name
            ] : null,
            'platform' => new PlatformResource($this->platform),
            'state' => [
                'id' => $this->state->id,
                'name' => $this->state->name
            ]
        ];
    }
}
