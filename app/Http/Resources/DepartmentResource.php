<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DepartmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
			'id' => $this->id,
			'name' => $this->name,
            'head' => [
                'worker_position' => $this->faculty->university->department_administrator_worker_position ? [
                    'id' => $this->faculty->university->department_administrator_worker_position->id,
                    'name' => $this->faculty->university->department_administrator_worker_position->name
                ] : null,
                'user' => $this->administrator ? [
                    'surname' => $this->administrator->surname,
                    'name' => $this->administrator->name,
                    'patronymic' => $this->administrator->patronymic
                ] : null
            ],
            'faculty' => [
                'id' => $this->faculty->id,
                'name' => $this->faculty->name,
                'university' => [
                    'id' => $this->faculty->university->id,
                    'name' => $this->faculty->university->name,
                    'faculty_type' => [
                        'id' => $this->faculty->university->faculty_type->id,
                        'name' => $this->faculty->university->faculty_type->name
                    ]
                ]
            ],
            'state' => [
                'id' => $this->state->id,
                'name' => $this->state->name
            ],
            'translations' => $this->translations()->get(['id', 'lang', 'name']),
        ];
    }
}
