<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DeviceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
			'id' => $this->id,
			'platform' => [
                'id' => $this->platform->id,
                'name' => $this->platform->name
            ],
            'brand' => $this->brand,
            'model' => $this->model,
            'imei' => $this->imei,
            'width' => $this->width,
            'height' => $this->height,
            'ip' => $this->ip,
            'created_at' => $this->created_at->format('Y-m-d H:i:s')
        ];
    }
}
