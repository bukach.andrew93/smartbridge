<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StatementSectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public static $wrap = 'result';
    
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->translation->name,
            'views' => StatementCategoryViewResource::collection($this->views)
        ];
    }
}
