<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClassroomTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'state' => [
                'id' => $this->state->id,
                'name' => $this->state->name
            ],
            'university' => [
                'id' => $this->university->id,
                'name' => $this->university->name
            ],
            'name' => $this->name,
            'translations' => $this->translations()->get(['id', 'lang', 'name']),
        ];
    }
}
