<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StudentServiceCenterResource extends JsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */

	public function toArray($request)
	{
		return [
			'id' => $this->id,
			'name' => $this->name,
			'accepting_applications_24_7' => $this->accepting_applications_24_7,
			'langs' => $this->langs,
			'operating_modes' => StudentServiceCenterOperatingMode::collection($this->operating_modes),
			'translations' => $this->translations,
			'head' => $this->head,
			'state' => [
				'id' => $this->state->id,
				'name' => $this->state->name,
			],
			'university' => [
				'id' => $this->university->id,
				'name' => $this->university->name
			]
		];
	}
}
