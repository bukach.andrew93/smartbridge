<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SpecialityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
			'id' => $this->id,
			'code' => $this->code,
			'name' => $this->name,
			'title' => ($this->code ? $this->code.' ' : '').$this->name,
			'value' => $this->id,
			'costs' => $this->costs,
			'department' => [
				'id' => $this->department->id,
				'name' => $this->department->name,
				'faculty' => [
	                'id' => $this->department->faculty->id,
	                'name' => $this->department->faculty->name,
	                'university' => [
	                    'id' => $this->department->faculty->university->id,
	                    'name' => $this->department->faculty->university->name,
	                    'speciality_type' => [
	                        'id' => $this->department->faculty->university->speciality_type->id,
	                        'name' => $this->department->faculty->university->speciality_type->name
	                    ]
	                ]
	            ],
			],
			'profession' => [
				'id' => $this->profession->id,
				'code' => $this->profession->code,
				'name' => $this->profession->name,
				'academic_degress' => AcademicDegreeResource::collection($this->profession->academicDegress),
				'departments' => DepartmentResource::collection($this->profession->departments)
			],
			'state' => [
                'id' => $this->state->id,
                'name' => $this->state->name
            ],
        ];
    }
}
