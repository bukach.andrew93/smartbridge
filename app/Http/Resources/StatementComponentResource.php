<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Models\Teacher;
use App\Models\Subject;
use App\Models\Speciality;
use App\Models\StudyLanguage;
use App\Models\User;

class StatementComponentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->type->slug == 'select' || $this->type->slug == 'select-multiple') {
            $options = $this->data;
        }
		
		if (auth()->user()->currentRole()->slug == 'student' && ($this->type->slug == 'resource' || $this->type->slug == 'resource-multiple')) {
			if ($this->resource->resource == 'student-teachers') {
				$teachers = User::query()
					->whereHas('teacher.card.studyGroups.students', function($query) {
						$query->where('id', auth()->user()->student->card->id);
					})
					->get();
				
 				$options = TeacherResource::collection($teachers);
			}
			
			if ($this->resource->resource == 'specialties') {
            	$options = SpecialityResource::collection(Speciality::query()->limit(40)->get());
			}
			
			if ($this->resource->resource == 'study-languages') {
				$study_languages = StudyLanguage::query()
					->whereHas('universities', function($query) {
						if (auth()->user()->currentRole()->slug == 'student') {
							$query->where('id', auth()->user()->student->card->group->speciality->department->faculty->university_id);
						}	
					})
					->get();
				
            	$options = StudyLanguageResource::collection($study_languages);
			}
			
			if ($this->resource->resource == 'student-subjects') {
				$subjects = Subject::query()
					->whereHas('studyGroup.students', function($query) {
						$query->where('user_id', auth()->user()->id);
					})
					->get();
				
            	$options = SubjectResource::collection($subjects);
			}
        }

        if (auth()->user()->currentRole()->slug == 'student') {
        	$resource = $this->resource->resource;
        }

        if (auth()->user()->currentRole()->slug == 'ssc-head' || auth()->user()->currentRole()->slug == 'ssc-administrator' || auth()->user()->currentRole()->slug == 'ssc-employee') {
        	$resource = $this->resource->resource;
        }

        return [
            'id' => $this->id,
			'type' => [
				'id' => $this->type->id,
				'slug' => $this->type->slug,
            	'name' => $this->type->name
			],
            'view_id' => $this->view_id,
            'name' => $this->name,
            'msg' => null,
			'file' => null,
			'files' => null,
            'rules' => $this->rules,
            'data' => [
                'options' => isset($options) ? $options : null
            ],
			'resource' => ($this->type->slug == 'resource' || $this->type->slug == 'resource-multiple') ? $this->resource->resource : null,
			'translations' => $this->translations
        ];
    }
}
