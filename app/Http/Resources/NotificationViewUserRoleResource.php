<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationViewUserRoleResource extends JsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */

	public function toArray($request)
	{
		return [
			'id' => $this->view->id,
			'icon' => asset($this->view->icon),
			'name' => $this->view->translation->name,
			'state' => (boolean)$this->state
		];
	}
}
