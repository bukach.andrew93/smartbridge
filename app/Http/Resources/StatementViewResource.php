<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use StatementComponentCollection;
//use StatementViewStageResource;

class StatementViewResource extends JsonResource
{
	/**
	 * Transform the resource into an array.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return array
	 */

	public function toArray($request)
	{
		return [
			'id' => $this->id,
			'category' => [
				'id' => $this->category->id,
				'name' => $this->category->name
			],
			'state' => [
				'id' => $this->state->id,
				'name' => $this->state->name
			],
			'responsible' => [
				'role' => [
					'id' => $this->role->id,
					'name' => $this->role->name
				],
				'user' => $this->user ? [
					'id' => $this->user->id,
					'surname' => $this->user->surname,
					'name' => $this->user->name,
					'patronymic' => $this->user->patronymic
				] : null
			],
			'name' => $this->name,
			'qr' => asset($this->qr),
			'template' => asset($this->template),
			'components' => StatementComponentResource::collection($this->components),
			'term_of_consideration' => $this->term_of_consideration,
			'instruction' => $this->instruction,
			'stages' => StatementViewStageResource::collection($this->stages),
			'translations' => $this->translations
		];
	}
}
