<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClassroomResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'campus' => [
                'id' => $this->campus->id,
                'name' => $this->campus->name
            ],
            'state' => [
                'id' => $this->state->id,
                'name' => $this->state->name
            ],
            'type' => [
                'id' => $this->type->id,
                'name' => $this->type->name
            ],
            'capacity' => $this->capacity,
            'name' => $this->name,
            'name' => $this->name,
            'name' => $this->name
        ];
    }
}
