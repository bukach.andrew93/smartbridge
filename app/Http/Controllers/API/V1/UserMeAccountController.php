<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\AccountResource;

use App\Models\Account;

class UserMeAccountController extends Controller
{
    public function index(Request $request)
    {
    	$accounts = Account::query()
    		->where('user_id', auth()->user()->id);

    	$accounts->whereHas('view.translation', function ($query) use ($request) {
			if ($request->has('filter.name')) {
				$query->where('name', 'like', '%'.trim($request->filter['name'], '*').'%');
			}
		});

		$accounts = $accounts->paginate();

    	return AccountResource::collection($accounts);
    }

    public function show(Account $account)
    {
		return new AccountResource($account);
    }
}
