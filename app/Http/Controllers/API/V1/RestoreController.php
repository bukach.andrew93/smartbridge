<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Lang;
use App\Models\Type;

use App\Mail\Restore;

use Validator;
use Mail;
use DB;
use UA;

class RestoreController extends Controller
{
    public function restore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email'
        ]);

        if ($validator->fails()) {
            return response()->json([
				'code' => 422,
                'messages' => $validator->messages() 
            ], 200);
        }
        
        $restore = DB::transaction(function() use ($request) {
            $user = User::query()
                ->where('email', $request->email)
                ->first();

            $restore = $user->emailRestores()->update([
                'fit' => '0'
            ]);

            $restore = $user->emailRestores()->create([
                'type_id' => Type::where('slug', $request->header('type'))->first()->id,
                'lang_id' => Lang::where('slug', app()->getLocale())->first()->id,
                'valid_until' => date('Y-m-d H:i:s', strtotime('+1 day')),
                'ip' => $request->server('REMOTE_ADDR'),
                'ua' => $request->server('HTTP_USER_AGENT'),
                'ua_device_brand' => UA::parse($request->server('HTTP_USER_AGENT'))->device->brand,
                'ua_device_model' => UA::parse($request->server('HTTP_USER_AGENT'))->device->model,
                'ua_os' => UA::parse($request->server('HTTP_USER_AGENT'))->os->family,
                'ua_os_major' => UA::parse($request->server('HTTP_USER_AGENT'))->os->major,
                'ua_os_minor' => UA::parse($request->server('HTTP_USER_AGENT'))->os->minor,
                'ua_browser' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->family,
                'ua_browser_major' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->major,
                'ua_browser_minor' => UA::parse($request->server('HTTP_USER_AGENT'))->ua->minor,
                'hash' => sha1(rand(1000, 9999)),
                'src' => $request->email
            ]);

            return $restore;
        });

        Mail::to($restore->src)
            ->locale($restore->user->defaultLang ? $restore->user->defaultLang->slug : 'ru')
            ->queue(new Restore($restore));

        return response()->json([
			'code' => 200
		], 200);
    }
}
