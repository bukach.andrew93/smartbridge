<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\PaymentViewResource;

use App\Models\PaymentView;

class UserMePaymentViewController extends Controller
{
    public function index()
    {
    	$payment_views = PaymentView::query();

    	$payment_views->with(['invoices' => function($query) {
    		$query->where('student_id', auth()->user()->student->card->id);
    		$query->where('status_id', 1);
    	}]);

    	$payment_views = $payment_views->paginate();

    	return PaymentViewResource::collection($payment_views);
    }
}
