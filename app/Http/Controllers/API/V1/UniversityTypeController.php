<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\UniversityTypeResource;

use App\Models\UniversityType;

use Validator;

class UniversityTypeController extends Controller
{
    public function index(Request $request)
    {
		$university_types = UniversityType::query();
		
		$university_types = $university_types->paginate();

    	return UniversityTypeResource::collection($university_types);
    }
}
