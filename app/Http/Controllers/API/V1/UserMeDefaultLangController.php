<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\LangResource;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Illuminate\Validation\Rule;

use App\Models\Lang;

use Validator;
use Mail;
use App;
use UA;
use DB;

class UserMeDefaultLangController extends Controller
{
	public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'default_lang_id' => [
				'required'
			]
        ]);

        if ($validator->fails()) {
            return response()->json([
                'messages' => $validator->messages(),
            ], 422);
        }
		
        auth()->user()->update([
            'default_lang_id' => $request->default_lang_id
        ]);

        return  response()->json([
			'code' => 200,
			'data' => new LangResource(auth()->user()->defaultLang)
		], 200);
    }
}
