<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use App\Http\Resources\StudentServiceCenterOperatingMode;
use App\Http\Resources\StatementRouteResource;

use App\Models\StatementStatus;
use App\Models\StatementStage;
use App\Models\StatementRoute;

use App\Events\StatementRouteProcessed;
use App\Events\StatementCompleted;

use Validator;
use Carbon\Carbon;
use DB;

class StatementRouteController extends Controller
{
    public function update(Request $request, StatementRoute $statement_route)
    {
		$student_service_center = auth()->user()->sscs[0];
		
		$current_date = date('Y-m-d');
		$current_time = date('H:i:s');
		
		$current_operating_mode = [
			'time_start' => $student_service_center->currentOperatingMode->time_start,
			'time_end' => $student_service_center->currentOperatingMode->time_end
		];
		
		if ($current_operating_mode['time_start'] == '00:00:00' && $current_operating_mode['time_end'] == '00:00:00') {
			return response()->json([
				'code' => 403,
				'data' => StudentServiceCenterOperatingMode::collection($student_service_center->operating_modes)
			], 200);
		}
		
		$validator = Validator::make($request->all(), [
			'status' => 'required|string|in:success,fail,processing',
			'message' => 'nullable|string|max:511',
			'files' => 'array',
			'files.*' => 'required|file|max:10240'
		]);

		if ($validator->fails()) {
			return response()->json([
				'code' => 422,
				'messages' => $validator->messages() 
			], 200);
		}
		
		//
		
		if ($statement_route->stage->statement->status->slug != 'processing' || ($request->status == 'processing' && $statement_route->status != 'waiting') || ($request->status != 'processing' && $statement_route->status != 'processing')) {
			return response()->json([
				'code' => 403 
			], 200);
		}
		
		//
		
		$statement_route = DB::transaction(function() use ($request, $statement_route) {
			$datetime_now = Carbon::now();

			$files = [];

			if ($request->has('files')) {
				foreach ($request->file('files') as $file) {
					$files[] = [
						'name' => $file->getClientOriginalName(),
						'src' => 'storage/'.Storage::putFileAs('uploads/'.date('Y/m/d/h/i/s'), $file, rand(1000, 9999).'.'.$file->extension())
					]; 
				}
			}
			
			$statement_route->update([
				'status' => $request->status,
				'responsible_user_id' => $statement_route->responsible_role_id == auth()->user()->studentServiceCenterRoles[0]->id ? auth()->user()->id : null,
				'message' => $request->message,
				'files' => count($files) > 0 ? $files : null,
				'started_at' => $request->status == 'processing' ? date('Y-m-d H:i:s') : $statement_route->started_at,
				'expired_at' => $statement_route->term ? $datetime_now->addHours($statement_route->term)->format('Y-m-d H:i:s') : null,
				'ended_at' => $request->status == 'success' || $request->status == 'fail' ? date('Y-m-d H:i:s') : $statement_route->ended_at
			]);
			
			if ($statement_route->stage->statement->status->slug == 'processing' && $statement_route->stage->statement->stages()->where('id', $statement_route->stage_id)->whereHas('routes', function($query){$query->whereIn('status', ['waiting', 'queue', 'processing']);})->count() == 0 && $statement_route->stage->processes != null) {
				foreach ($statement_route->stage->processes as $process) {
					if (eval('return '.implode(' ', [$process['if']['left'],$process['if']['operator'],$process['if']['right']]).';')) {
						eval('return '.$process['then'].';');
					} elseif (isset($process['else']) && !empty($process['else'])) {
						eval('return '.$process['else'].';');
					}
				}
			}
			
			$statement_route = $statement_route::find($statement_route->id);
			
			if ($statement_route->stage->statement->status->slug == 'processing' && $statement_route->stage->statement->stages()->where('id', $statement_route->stage_id)->whereHas('routes', function($query){$query->whereIn('status', ['waiting', 'queue', 'processing']);})->count() == 0) {
				foreach ($statement_route->stage->statement->stages()->where('id', '<>', $statement_route->stage_id)->whereHas('routes', function($query){$query->whereIn('status', ['waiting', 'queue']);})->get() as $stage) {
					if ($stage->conditions) {
						$conditions = [];

						foreach ($stage->conditions as $condition) {
							$conditions[] = $condition['value'];
						}

						if (eval('return '.implode('&&', $conditions).';')) {
							foreach ($stage->routes as $route) {
								if ($route->responsible_type == 'responsible') {
									$route->update([
										'responsible_user_id' => $statement->responsible_user_id,
										'status' => 'processing',
										'delivered_at' => $datetime_now->format('Y-m-d H:i:s'),
										//	'started_at' => $datetime_now->format('Y-m-d H:i:s'),
										'expired_at' => $route->term ? $datetime_now->addHours($route->term)->format('Y-m-d H:i:s') : null
									]);
								}
								
								if ($route->responsible_user_id) {
									$route->update([
										'status' => 'processing',
										'delivered_at' => date('Y-m-d H:i:s'),
										//	'started_at' => date('Y-m-d H:i:s'),
										'expired_at' => $route->term ? $datetime_now->addHours($route->term)->format('Y-m-d H:i:s') : null
									]);
								}
								
								if (!$route->responsible_user_id) {
									$route->update([
										'status' => 'waiting',
										'delivered_at' => date('Y-m-d H:i:s'),
										'expired_at' => $route->term ? $datetime_now->addHours($route->term)->format('Y-m-d H:i:s') : null
									]);
								}
							}
						} else {
							$stage->routes()->update([
								'status' => 'pass'
							]);
						}
					}
					
					if (!$stage->conditions) {
						foreach ($stage->routes as $route) {
							if ($route->statement_responsible_user == '1') {
								$route->update([
									'responsible_user_id' => $statement->responsible_user_id,
									//	'status' => 'processing',
									'status' => 'waiting',
									'delivered_at' => date('Y-m-d H:i:s'),
									//	'started_at' => date('Y-m-d H:i:s'),
									'expired_at' => $route->term ? $datetime_now->addHours($route->term)->format('Y-m-d H:i:s') : null
								]);
							}
								
							if ($route->responsible_user_id) {
								$route->update([
									//	'status' => 'processing',
									'status' => 'waiting',
									'delivered_at' => date('Y-m-d H:i:s'),
									//	'started_at' => date('Y-m-d H:i:s'),
									'expired_at' => $route->term ? $datetime_now->addHours($route->term)->format('Y-m-d H:i:s') : null
								]);
							}
								
							if (!$route->responsible_user_id) {
								$route->update([
									'status' => 'waiting',
									'delivered_at' => date('Y-m-d H:i:s'),
									'expired_at' => $route->term ? $datetime_now->addHours($route->term)->format('Y-m-d H:i:s') : null
								]);
							}
						}	
					}
					
					if ($stage->routes()->whereIn('status', ['waiting', 'processing'])->count() > 0) {
						break;
					}
				}
			}
			
			return $statement_route;
		});
		
		StatementRouteProcessed::dispatch($statement_route);

		/*

		if ($statement_route->stage->statement->status->slug == 'ready' || $statement_route->stage->statement->status->slug == 'renouncement') {
			StatementCompleted::dispatch($statement_route->stage->statement);
		}

		*/
		
		return response()->json([
			'code' => 200,
			'data' => new StatementRouteResource($statement_route)
		], 200);
    }
}
