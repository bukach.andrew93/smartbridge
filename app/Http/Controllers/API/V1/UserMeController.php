<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\UserResource;

use App\Http\Resources\ScheduleResource;

use App\Models\Timetable;

class UserMeController extends Controller
{
    public function schedule(Request $request)
    {
    	$timetables = Timetable::query()
    		->whereHas('studyGroup.students', function ($query) {
    			$query->where('user_id', auth()->user()->id);
    		});
		
		if ($request->has('filter.week')) {
			$timetables->where('week_number', $request->filter['week']);
		} else {
			$timetables->where('week_number', 1);
		}
	
		$timetables = $timetables->orderBy('lesson_hour_id', 'asc')
    		->paginate();
		
		$weeks = Timetable::query()
			->whereHas('studyGroup.students', function ($query) {
    			$query->where('user_id', auth()->user()->id);
    		})
			->select('week_number')
			->distinct()
			->get();

    	return (ScheduleResource::collection($timetables))->additional([
			'meta' => [
				'current' => [
					'week' => $request->has('filter.week') ? $request->filter['week'] : 1
				],
				'weeks' => $weeks
			]
		]);
    }


    public function me()
    {
        return [
			'code' => 200,
			'data' => new UserResource(auth()->user())
		];
    }
}
