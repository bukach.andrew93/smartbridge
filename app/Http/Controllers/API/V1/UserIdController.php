<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserIdController extends Controller
{
    public function store(Request $request, User $user)
    {
    	$validator = Validator::make($request->all(), [
            'type_id' => 'required|integer|exists:id_types,id', 
	        'citizenship_id' => [
	        	'required',
	        	'integer',
	        	Rule::exists('id_types_citizenships')->where(function ($query) use ($request){
	        		return $query->where('id_type_id', $request->type_id)->where('citizenship_id', $request->citizenship_id);
	        	})
	        ],
	        'issuing_authority_id' => [
	        	'required',
	        	'integer',
	        	Rule::exists('id_issuing_authorities')->where(function ($query) use ($request){
	        		return $query->where('id', $request->issuing_authority_id)->where('citizenship_id', $request->citizenship_id);
	        	})
	        ],
	        'date_start' => 'required|date_format:Y-m-d', 
	        'date_end' => 'required|date_format:Y-m-d',
	        'series' => 'required|string|max:255',
	        'number' => 'required|string|max:255',
	        'tin' => 'nullable|string|max:255',
	        'src' => 'required|file|mimes:pdf|max:5120'
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages()
            ], 422);
        }

        $user->hpeds()->create([
        	'type_id' => $request->type_id,
        	'citizenship_id' => $request->citizenship_id,
        	'issuing_authority_id' => $request->issuing_authority_id,
	        'date_start' => $request->date_start,
	        'date_end' => $request->date_end,
	        'series' => $request->series,
	        'number' => $request->number,
	        'tin' => $request->tin,
	        'src' => Storage::put('uploads/'.date('Y/m/d/h/i/s'), $request->src)
        ]);

        return response()->json(null, 201);
    }
}
