<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\IntegrationResource;

use App\Models\Integration;

use Validator;

class IntegrationController extends Controller
{
    public function index(Request $request)
    {
        $integrations = Integration::query();

        if ($request->has('orderBy') && $request->orderBy['id']) {
            $integrations->orderBy('id', $request->orderBy['id']);
        }

        $integrations = $integrations->paginate();

        return IntegrationResource::collection($integrations);
    }
}
