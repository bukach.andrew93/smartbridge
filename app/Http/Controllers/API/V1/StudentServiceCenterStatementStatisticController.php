<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use App\Http\Resources\StatementCategoryStatisticResource;
use App\Http\Resources\StatementViewStatisticResource;

use App\Models\StatementCategory;
use App\Models\StatementStatus;
use App\Models\StatementView;
use App\Models\Statement;
use App\Models\Month;

use Carbon\Carbon;

use \ConvertApi\ConvertApi;

use Validator;

class StudentServiceCenterStatementStatisticController extends Controller
{
	public function request(Request $request)
    {
		$validator = Validator::make($request->all(), [
            'groupBy' => [
				'required',
				'in:day,month'
			],
            'interval' => [
				'required',
				'in:last7days,last30days,period'
			],
			'filter.date_start' => [
				'required_if:interval,period'
			],
			'filter.date_end' => [
				'required_if:interval,period',
			]
        ]);

        if ($validator->fails()) {
            return response()->json([
				'code' => 422,
            	'messages' => $validator->messages()
            ], 200);
        }
		
		$data = collect();
		
		$statements = Statement::query()
			->has('view');
		
		if ($request->interval == 'last30days') {
			$statements->whereBetween('created_at', [
				Carbon::now()->subDays(30)->format('Y-m-d').' 00:00:00',
				Carbon::now()->format('Y-m-d').' 23:59:59'
			]);
		}
		
		if ($request->interval == 'last7days') {
			$statements->whereBetween('created_at', [
				Carbon::now()->subDays(7)->format('Y-m-d').' 00:00:00',
				Carbon::now()->format('Y-m-d').' 23:59:59'
			]);
		}
		
		if ($request->interval == 'period') {
			$statements->whereBetween('created_at', [
				$request->filter['date_start'].' 00:00:00',
				$request->filter['date_end'].' 23:59:59'
			]);
		}
		
		//	Day
		
		if ($request->groupBy == 'day' && $request->interval == 'period') {
			$meta['date'] = [
				'start' => $request->filter['date_start'],
				'end' => $request->filter['date_end']
			];
			
			for ($date = Carbon::parse($request->filter['date_start']); $date->lte(Carbon::parse($request->filter['date_end'])); $date->addDay()) {
				$statement = Statement::query()
					->where('created_at', 'LIKE', $date->format('Y-m-d').'%');
				
				//	Role
		
				if (auth()->user()->currentRole()->slug == 'ssc-employee') {
					$statement->whereHas('view.category', function($query) {
						$query->where('ssc_id', auth()->user()->sscs[0]->id);
					});
				}
				
				if (auth()->user()->currentRole()->slug == 'ssc-administrator') {
					$statement->whereHas('view.category', function($query) {
						$query->where('ssc_id', auth()->user()->sscs[0]->id);
					});
				}

				if (auth()->user()->currentRole()->slug == 'ssc-head') {
					$statement->whereHas('view.category', function($query) {
						$query->where('ssc_id', auth()->user()->sscs[0]->id);
					});
				}
				
				$data->push([
					'date' => $date->format('Y-m-d'),
					'count' => $statement->count()
				]);
			 }
		}
		
		if ($request->groupBy == 'day' && $request->interval == 'last7days') {
			$meta['date'] = [
				'start' => Carbon::now()->subDays(7)->format('Y-m-d'),
				'end' => Carbon::now()->format('Y-m-d')
			];
			
			for ($date = Carbon::parse(Carbon::now()->subDays(7)->format('Y-m-d')); $date->lte(Carbon::parse(Carbon::now()->format('Y-m-d'))); $date->addDay()) {
				
				$statement = Statement::query()
					->where('created_at', 'LIKE', $date->format('Y-m-d').'%');
				
				//	Role
		
				if (auth()->user()->currentRole()->slug == 'ssc-employee') {
					$statement->whereHas('view.category', function($query) {
						$query->where('ssc_id', auth()->user()->sscs[0]->id);
					});
				}

				if (auth()->user()->currentRole()->slug == 'ssc-administrator') {
					$statement->whereHas('view.category', function($query) {
						$query->where('ssc_id', auth()->user()->sscs[0]->id);
					});
				}

				if (auth()->user()->currentRole()->slug == 'ssc-head') {
					$statement->whereHas('view.category', function($query) {
						$query->where('ssc_id', auth()->user()->sscs[0]->id);
					});
				}
				
				 $data->push([
					'date' => $date->format('Y-m-d'),
					'count' => $statement->count()
				 ]);
			 }
		}
		
		//	Day
		
		if ($request->groupBy == 'month' && $request->interval == 'period') {
			$meta['date'] = [
				'start' => $request->filter['date_start'],
				'end' => $request->filter['date_end']
			];
			
			for ($date = Carbon::parse($request->filter['date_start']); $date->lte(Carbon::parse($request->filter['date_end'])); $date->addMonth()) {
				$statement = Statement::query()
					->where('created_at', 'LIKE', $date->format('Y-m').'%');
				
				//	Role
		
				if (auth()->user()->currentRole()->slug == 'ssc-employee') {
					$statement->whereHas('view.category', function($query) {
						$query->where('ssc_id', auth()->user()->sscs[0]->id);
					});
				}
				
				if (auth()->user()->currentRole()->slug == 'ssc-administrator') {
					$statement->whereHas('view.category', function($query) {
						$query->where('ssc_id', auth()->user()->sscs[0]->id);
					});
				}

				if (auth()->user()->currentRole()->slug == 'ssc-head') {
					$statement->whereHas('view.category', function($query) {
						$query->where('ssc_id', auth()->user()->sscs[0]->id);
					});
				}
				
				$data->push([
					'date' => $date->format('Y-m'),
					'count' => $statement->count()
				]);
			 }
		}
		
		$statements = $statements->get();
		
		return response()->json([
			'code' => 200,
			'data' => $data,
			'meta' => $meta
		], 200);
    }
	
    public function category(Request $request)
    {
		$validator = Validator::make($request->all(), [
            'interval' => [
				'required',
				'in:last7days,period'
			],
			'filter.date_start' => [
				'required_if:interval,period'
			],
			'filter.date_end' => [
				'required_if:interval,period',
			]
        ]);

        if ($validator->fails()) {
            return response()->json([
				'code' => 422,
            	'messages' => $validator->messages()
            ], 200);
        }
		
		$categories = StatementCategory::query()
			->has('translation')
			->whereDoesntHave('childrens');
		
		//	Role

		if (auth()->user()->currentRole()->slug == 'ssc-head') {
			$categories->where('ssc_id', auth()->user()->sscs[0]->id);
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-administrator') {
			$categories->where('ssc_id', auth()->user()->sscs[0]->id);
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-employee') {
			$categories->where('ssc_id', auth()->user()->sscs[0]->id);
		}
		
		$categories = $categories->get();
		
		$data = collect();
		$date = [];
		
		$total = 0;
		
		if ($request->interval == 'last7days') {
			$date = [
				'start' => Carbon::now()->subDays(7)->format('Y-m-d'),
				'end' => Carbon::now()->format('Y-m-d')
			];
		}

		if ($request->interval == 'period') {
			$date = [
				'start' => $request->filter['date_start'],
				'end' => $request->filter['date_end']
			];
		}
		
		foreach ($categories as $category) {
			$statements = Statement::query()
				->whereHas('view', function($query) use ($category) {
					$query->where('category_id', $category->id);
				})
				->whereBetween('created_at', [
					$date['start'].' 00:00:00',
					$date['end'].' 23:59:59'
				]);
			
			//	Role
		
			if (auth()->user()->currentRole()->slug == 'ssc-employee') {
				$statements->whereHas('view.category', function($query) {
					$query->where('ssc_id', auth()->user()->sscs[0]->id);
				});
			}
			
			if (auth()->user()->currentRole()->slug == 'ssc-administrator') {
				$statements->whereHas('view.category', function($query) {
					$query->where('ssc_id', auth()->user()->sscs[0]->id);
				});
			}

			if (auth()->user()->currentRole()->slug == 'ssc-head') {
				$statements->whereHas('view.category', function($query) {
					$query->where('ssc_id', auth()->user()->sscs[0]->id);
				});
			}
			
			$statements = $statements->get();
			
			$data->push([
				'name' => $category->name,
				'count' => $statements->count()
			]);
			
			$total += $statements->count();
		}
		
		return response()->json([
			'code' => 200,
			'data' => $data->sortByDesc('count')->values()->all(),
			'meta' => [
				'date' => [
					'start' => '2020-01-01',
					'end' => '2020-03-01'
				],
				'total' => $total
			]
		], 200);
		
		//	return StatementCategoryStatisticResource::collection($statement_categories);
    }
	
	public function view(Request $request)
    {
		$validator = Validator::make($request->all(), [
            'interval' => [
				'required',
				'in:last7days,period'
			],
			'filter.date_start' => [
				'required_if:interval,period'
			],
			'filter.date_end' => [
				'required_if:interval,period',
			]
        ]);

        if ($validator->fails()) {
            return response()->json([
				'code' => 422,
            	'messages' => $validator->messages()
            ], 200);
        }
		
		$views = StatementView::query()
			->has('translation');
		
		//	Role

		if (auth()->user()->currentRole()->slug == 'ssc-head') {
			$views->whereHas('category', function($query) {
				$query->where('ssc_id', auth()->user()->sscs[0]->id);
			});
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-administrator') {
			$views->whereHas('category', function($query) {
				$query->where('ssc_id', auth()->user()->sscs[0]->id);
			});
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-employee') {
			$views->whereHas('category', function($query) {
				$query->where('ssc_id', auth()->user()->sscs[0]->id);
			});
		}
		
		$views = $views->get();
		
		$data = collect();
		$date = [];
		
		$total = 0;
		
		if ($request->interval == 'last7days') {
			$date = [
				'start' => Carbon::now()->subDays(7)->format('Y-m-d'),
				'end' => Carbon::now()->format('Y-m-d')
			];
		}

		if ($request->interval == 'period') {
			$date = [
				'start' => $request->filter['date_start'],
				'end' => $request->filter['date_end']
			];
		}
		
		foreach ($views as $view) {
			$statements = Statement::query()
				->where('view_id', $view->id)
				->whereBetween('created_at', [
					$date['start'].' 00:00:00',
					$date['end'].' 23:59:59'
				]);
		
			//	Role

			if (auth()->user()->currentRole()->slug == 'ssc-head') {
				$statements->whereHas('view.category', function($query) {
					$query->where('ssc_id', auth()->user()->sscs[0]->id);
				});
			}
		
			if (auth()->user()->currentRole()->slug == 'ssc-administrator') {
				$statements->whereHas('view.category', function($query) {
					$query->where('ssc_id', auth()->user()->sscs[0]->id);
				});
			}
		
			if (auth()->user()->currentRole()->slug == 'ssc-employee') {
				$statements->whereHas('view.category', function($query) {
					$query->where('ssc_id', auth()->user()->sscs[0]->id);
				});
			}
			
			$statements = $statements->get();
			
			$data->push([
				'name' => $view->name,
				'count' => $statements->count()
			]);
			
			$total += $statements->count();
		}
		
		return response()->json([
			'code' => 200,
			'data' => $data->sortByDesc('count')->values()->all(),
			'meta' => [
				'date' => [
					'start' => $date['start'],
					'end' => $date['end']
				],
				'total' => $total
			]
		], 200);
    }
	
	public function status(Request $request)
	{
		$validator = Validator::make($request->all(), [
            'interval' => [
				'required',
				'in:last7days,period'
			],
			'filter.date_start' => [
				'required_if:interval,period'
			],
			'filter.date_end' => [
				'required_if:interval,period',
			]
        ]);

        if ($validator->fails()) {
            return response()->json([
				'code' => 422,
            	'messages' => $validator->messages()
            ], 200);
        }
		
		$statuses = StatementStatus::query()
			->get();
		
		$data = collect();
		$date = [];
		
		$total = 0;
		
		if ($request->interval == 'last7days') {
			$date = [
				'start' => Carbon::now()->subDays(7)->format('Y-m-d'),
				'end' => Carbon::now()->format('Y-m-d')
			];
		}

		if ($request->interval == 'period') {
			$date = [
				'start' => $request->filter['date_start'],
				'end' => $request->filter['date_end']
			];
		}
		
		foreach ($statuses as $status) {
			$statements = Statement::query()
				->where('status_id', $status->id)
				->whereBetween('created_at', [
					$date['start'].' 00:00:00',
					$date['end'].' 23:59:59'
				]);
			
			//	Role

			if (auth()->user()->currentRole()->slug == 'ssc-head') {
				$statements->whereHas('view.category', function($query) {
					$query->where('ssc_id', auth()->user()->sscs[0]->id);
				});
			}
		
			if (auth()->user()->currentRole()->slug == 'ssc-administrator') {
				$statements->whereHas('view.category', function($query) {
					$query->where('ssc_id', auth()->user()->sscs[0]->id);
				});
			}
		
			if (auth()->user()->currentRole()->slug == 'ssc-employee') {
				$statements->whereHas('view.category', function($query) {
					$query->where('ssc_id', auth()->user()->sscs[0]->id);
				});
			}
			
			$statements = $statements->get();
			
			$data->push([
				'slug' => $status->slug,
				'name' => $status->name,
				'count' => $statements->count()
			]);
			
			$total += $statements->count();
		}
		
		return response()->json([
			'code' => 200,
			'data' => $data,
			'meta' => [
				'date' => [
					'start' => $date['start'],
					'end' => $date['end']
				],
				'total' => $total
			]
		], 200);
	}
	
	public function report(Request $request)
	{
		$validator = Validator::make($request->all(), [
            'interval' => [
				'required',
				'in:last7days,period'
			],
			'filter.date_start' => [
				'required_if:interval,period'
			],
			'filter.date_end' => [
				'required_if:interval,period',
			]
        ]);

        if ($validator->fails()) {
            return response()->json([
				'code' => 422,
            	'messages' => $validator->messages()
            ], 200);
        }
		
		$total = 0;
		$values = [];
		
		if ($request->interval == 'last7days') {
			$date = [
				'start' => Carbon::now()->subDays(7)->format('Y-m-d'),
				'end' => Carbon::now()->format('Y-m-d')
			];
		}

		if ($request->interval == 'period') {
			$date = [
				'start' => $request->filter['date_start'],
				'end' => $request->filter['date_end']
			];
		}
		
		$document = new \PhpOffice\PhpWord\TemplateProcessor(asset('report.statements.docx'));
		
		$document->setValue('date_start', $date['start']);
		$document->setValue('date_end', $date['end']);
		
		$statuses = StatementStatus::query()
			->get();
		
		foreach ($statuses as $status) {
			$statements = Statement::query()
				->where('status_id', $status->id)
				->whereBetween('created_at', [
					$date['start'].' 00:00:00',
					$date['end'].' 23:59:59'
				]);
		
			//	Role

			if (auth()->user()->currentRole()->slug == 'ssc-head') {
				$statements->whereHas('view.category', function($query) {
					$query->where('ssc_id', auth()->user()->sscs[0]->id);
				});
			}
		
			if (auth()->user()->currentRole()->slug == 'ssc-administrator') {
				$statements->whereHas('view.category', function($query) {
					$query->where('ssc_id', auth()->user()->sscs[0]->id);
				});
			}
		
			if (auth()->user()->currentRole()->slug == 'ssc-employee') {
				$statements->whereHas('view.category', function($query) {
					$query->where('ssc_id', auth()->user()->sscs[0]->id);
				});
			}
			
			$statements = $statements->get();
			
			if ($status->slug == 'waiting') {
				$document->setValue('waiting.name', $status->name);
				$document->setValue('waiting.count', $statements->count());
			}
			
			if ($status->slug == 'processing') {
				$document->setValue('processing.name', $status->name);
				$document->setValue('processing.count', $statements->count());
			}
			
			if ($status->slug == 'ready') {
				$document->setValue('ready.name', $status->name);
				$document->setValue('ready.count', $statements->count());
			}
			
			if ($status->slug == 'renouncement') {
				$document->setValue('renouncement.name', $status->name);
				$document->setValue('renouncement.count', $statements->count());
			}
		
			$total += $statements->count();
		}
			
		$document->setValue('view.total', $total);
		
		$views = StatementView::query();
			
		//	Role

		if (auth()->user()->currentRole()->slug == 'ssc-head') {
			$views->whereHas('category', function($query) {
				$query->where('ssc_id', auth()->user()->sscs[0]->id);
			});
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-administrator') {
			$views->whereHas('category', function($query) {
				$query->where('ssc_id', auth()->user()->sscs[0]->id);
			});
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-employee') {
			$views->whereHas('category', function($query) {
				$query->where('ssc_id', auth()->user()->sscs[0]->id);
			});
		}
			
		$views = $views->get();
		
		$data = collect();
		
		foreach ($views as $view) {
			$statements = Statement::query()
				->where('view_id', $view->id)
				->whereBetween('created_at', [
					$date['start'].' 00:00:00',
					$date['end'].' 23:59:59'
				]);
			
			//	Role

			if (auth()->user()->currentRole()->slug == 'ssc-head') {
				$statements->whereHas('view.category', function($query) {
					$query->where('ssc_id', auth()->user()->sscs[0]->id);
				});
			}
		
			if (auth()->user()->currentRole()->slug == 'ssc-administrator') {
				$statements->whereHas('view.category', function($query) {
					$query->where('ssc_id', auth()->user()->sscs[0]->id);
				});
			}
		
			if (auth()->user()->currentRole()->slug == 'ssc-employee') {
				$statements->whereHas('view.category', function($query) {
					$query->where('ssc_id', auth()->user()->sscs[0]->id);
				});
			}
			
			$statements = $statements->get();
			
			$data->push([
				'name' => $view->name,
				'count' => $statements->count()
			]);
		}
		
		if ($data->count() > 0) {
			foreach ($data->sortByDesc('count')->values()->all() as $view) {
				$values[] = [
					'view.name' => $view['name'],
			 		'view.count' => $view['count']
				];
			}
		}
			
		$document->cloneRowAndSetValues('view.name', $values);
			
		$directory = 'pdfs/'.date('Y/m/d/H/i/s');

		Storage::makeDirectory($directory);

		$src = $directory.'/'.rand(100000, 999999);

		$document->saveAs('storage/'.$src.'.docx');

		ConvertApi::setApiSecret('dtV6otX94Bugy01z');

		$result = ConvertApi::convert('pdf', [
			'File' => asset('storage/'.$src.'.docx')
		], 'doc');

		$result->saveFiles('storage/'.$src.'.pdf');
			
		return response()->json([
			'code' => 200,
			'link' => asset('storage/'.$src.'.pdf')
		], 200);
	}
}
