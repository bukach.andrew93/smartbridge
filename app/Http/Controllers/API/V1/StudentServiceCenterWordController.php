<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use App\Http\Resources\WordResource;

use App\Models\Word;

use Validator;

class StudentServiceCenterWordController extends Controller
{
   public function update(Request $request, Word $word)
    {
		$ssc = auth()->user()->sscs[0];

		$validator = Validator::make($request->all(), [
			'key' => [
				'required',
				'string',
				'max:1024',
				Rule::unique('words', 'key')->where(function ($query) use ($request, $ssc){
					$query->where('title', $request->title)
						->where('value', $request->value)
						->where('university_id', $ssc->university->id);
				})->ignore($word->id ,'id')
			],
            'title' => 'required|string|max:1024',
			'value' => 'required|string|max:1024'
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages()
            ], 422);
        }
		
		if (auth()->user()->currentRole()->slug == 'ssc-head' || auth()->user()->currentRole()->slug == 'ssc-administrator' || auth()->user()->currentRole()->slug == 'ssc-employee') {
			$university = auth()->user()->sscs[0]->university;
		}
		
		$word->update([
			'university_id' => $university ? $university->id : null,
			'key' => $request->key,
            'title' => $request->title,
			'value' => $request->value
		]);
		
		return response()->json([
			'code' => 200,
			'data' => new WordResource($word)
		], 200);
    }
	
	public function destroy(Request $request, Word $word)
    {
		$word->delete();
		
    	return response()->json([
			'code' => 204
		], 200);
    }
}
