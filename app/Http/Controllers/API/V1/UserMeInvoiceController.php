<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use App\Http\Resources\InvoiceResource;

use App\Models\TransactionView;
use App\Models\InvoiceStatus;
use App\Models\Transaction;
use App\Models\Invoice;

use Validator;
use DB;

class UserMeInvoiceController extends Controller
{
    public function index(Request $request)
    {
        $invoices = Invoice::query()
            ->whereHas('account', function($query) {
                $query->where('user_id', auth()->user()->id);
            });

        if ($request->has('filter.status')) {
            $invoices->whereHas('status', function($query) use ($request) {
                $query->where('slug', $request->filter['status']);
            });
        }

        $invoices = $invoices->paginate();

        return InvoiceResource::collection($invoices);
    }

    public function show(Request $request, Invoice $invoice)
    {
        return new InvoiceResource($invoice);
    }

    public function update(Request $request, Invoice $invoice)
    {
        $validator = Validator::make($request->all(), [
            'sum' => [
                'required',
                'numeric',
                'min:1',
                'lte:'.($invoice->debt - $invoice->paid),
                'max:'.auth()->user()->account->balance
            ]
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 422,
                'messages' => $validator->messages() 
            ], 200);
        }

        $data = DB::transaction(function() use ($request, $invoice) {
            auth()->user()->account()->update([
                'balance' => auth()->user()->account->balance - $request->sum
            ]);

            $transaction = auth()->user()->account->transactions()->create([
                'view_id' => TransactionView::where('slug', 'payment')->first()->id,
                'sum' => $request->sum
            ]);

            $transaction->payment()->create([
                'view_id' => $invoice->payment_view_id
            ]);

            //  Сумма равно долгу

            if ($request->sum == ($invoice->debt - $invoice->paid)) {
                $invoice->update([
                    'paid' => $invoice->paid + $request->sum,
                    'status_id' => InvoiceStatus::where('slug', 'paid')->first()->id
                ]);
            }

            //  Сумма меньше долга

            if ($request->sum < ($invoice->debt - $invoice->paid)) {
                $invoice->update([
                    'paid' => $invoice->paid + $request->sum
                ]);
            }
        
            $invoice->payments()->attach($transaction->payment, [
                'sum' => $request->sum
            ]);

            return $invoice;
        });

        return response()->json([
            'code' => 200,
            'data' => $data
        ], 200);
    }
}
