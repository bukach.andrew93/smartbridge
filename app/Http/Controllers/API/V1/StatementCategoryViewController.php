<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\StatementCategoryResource;

use App\Models\StatementCategory;

use Validator;
use DB;

class StatementCategoryViewController extends Controller
{
    public function index(Request $request, StatementCategory $statement_category)
    {
		$statement_category = StatementCategory::query()
			->where('id', $statement_category->id);
		
    	$statement_category->with(['views' => function ($query) use ($request) {
			if ($request->has('filter.state_id')) {
				$query->where('state_id', $request->filter['state_id']);
			}

			$query->has('translation');
		}]);
		
        $statement_category = $statement_category->first();
		
        return new StatementCategoryResource($statement_category);
    }
}
