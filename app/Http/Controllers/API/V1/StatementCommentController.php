<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use Illuminate\Validation\Rule;

use App\Http\Resources\StatementCommentResource;

use App\Events\StatementCommentCreated;

use App\Models\StatementComment;

use Validator;
use DB;

class StatementCommentController extends Controller
{
	public function index(Request $request)
	{
		$statement_comments = StatementComment::query();

		if ($request->has('filter.statement_id')) {
			$statement_comments->where('statement_id', $request->filter['statement_id']);
		}

		$statement_comments = $statement_comments->paginate(100);

		return StatementCommentResource::collection($statement_comments);
	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'statement_id' => 'required|exists:statements,id',
			'message' => 'required|string|max:5000',
			'files' => 'array',
			'files.*' => 'required|file|max:10240'
		]);

		if ($validator->fails()) {
			return response()->json([
				'code' => 422,
				'messages' => $validator->messages() 
			], 200);
		}
		
		$statement_comment = DB::transaction(function() use ($request) {
			$files = [];

			if ($request->has('files')) {
				foreach ($request->file('files') as $file) {
					$files[] = [
						'name' => $file->getClientOriginalName(),
						'src' => 'storage/'.Storage::putFileAs('uploads/'.date('Y/m/d/h/i/s'), $file, rand(1000, 9999).'.'.$file->extension())
					]; 
				}
			}

			$statement_comment = StatementComment::create([
				'statement_id' => $request->statement_id,
				'user_id' => auth()->user()->id,
				'message' => $request->message,
				'files' => count($files) > 0 ? $files : null
			]);

			return $statement_comment;
		});

		StatementCommentCreated::dispatch($statement_comment);

		return response()->json([
			'code' => 201,
			'data' => new StatementCommentResource($statement_comment)
		], 200);
	}
 }
