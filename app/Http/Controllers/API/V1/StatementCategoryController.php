<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use App\Http\Resources\StatementCategoryResource;

use App\Models\StatementCategoryLang;
use App\Models\StudentServiceCenter;
use App\Models\StatementCategory;

use Validator;
use DB;

class StatementCategoryController extends Controller
{
    public function index(Request $request)
    {	
    	$statement_categories = StatementCategory::query()
			->has('translation')
			->with(['views' => function($query) {
				$query->has('translation');
			}]);

        if (auth()->user()->currentRole()->slug == 'ssc-head') {
			$statement_categories->where('ssc_id', auth()->user()->sscs[0]->id);
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-administrator') {
			$statement_categories->where('ssc_id', auth()->user()->sscs[0]->id);
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-employee') {
			$statement_categories->where('ssc_id', auth()->user()->sscs[0]->id);
		}
		
		if (auth()->user()->currentRole()->slug == 'student') {
			$statement_categories->where('ssc_id', auth()->user()->student->card->group->speciality->department->faculty->university->studentServiceCenter ? auth()->user()->student->card->group->speciality->department->faculty->university->studentServiceCenter->id : null);
		}
		
		//  filter

		if ($request->has('filter_not.id')) {
			$statement_categories->whereNotIn('id', explode(',', $request->filter_not['id']));
		}

		if ($request->has('filter_not.views') && $request->filter_not['views'] == 'null') {
			$statement_categories->doesntHave('views');
		}

		if ($request->has('filter_not.childrens') && $request->filter_not['childrens'] == 'null') {
			$statement_categories->doesntHave('childrens');
		}
		
		if ($request->has('filter.parent_id')) {
			$statement_categories->where('parent_id', $request->filter['parent_id']);
		}
		
		if ($request->has('filter.state_id')) {
			$statement_categories->where('state_id', $request->filter['state_id']);
		}

		if ($request->has('filter.name')) {
			$statement_categories->whereHas('translation', function ($query) use ($request) {
				$query->where('name', 'like', $request->filter['name']);
			});
		}

        $statement_categories = $statement_categories->paginate();

        return StatementCategoryResource::collection($statement_categories);
    }

    public function show(StatementCategory $statement_category)
    {
		$statement_category = StatementCategory::query()
			->has('translation')
			->with(['views' => function($query) {
				$query->has('translation');
			}])
			->with(['childrens' => function($query) {
				$query->has('translation');
			}])
			->where('id', $statement_category->id)
			->first();
		
        return new StatementCategoryResource($statement_category);
    }
	
	public function store(Request $request)
	{
		$ssc = auth()->user()->sscs[0];
		
		$validator = Validator::make($request->all(), [
			'parent_id' => [
				'exists:statement_categories,id'
			],
			'state_id' => 'required|exists:states,id',
			'translations' => 'required',
			'translations.kz.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('kz');
				}),
				'string',
				'max:255',
				Rule::unique('statement_category_langs', 'name')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'kz')->whereIn('id', $ssc->categories()->pluck('id'));
				})
			],
			'translations.ru.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('ru');
				}),
				'string',
				'max:255',
				Rule::unique('statement_category_langs', 'name')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'ru')->whereIn('id', $ssc->categories()->pluck('id'));
				})
			],
			'translations.en.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('en');
				}),
				'string',
				'max:255',
				Rule::unique('statement_category_langs', 'name')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'en')->whereIn('id', $ssc->categories()->pluck('id'));
				})
			],
		]);

		if ($validator->fails()) {
			return response()->json([
				'code' => 422,
				'messages' => $validator->messages() 
			], 200);
		}

		$data = DB::transaction(function() use ($request, $ssc) {
			$statement_category = StatementCategory::create([
				'ssc_id' => $ssc->id,
				'parent_id' => $request->parent_id,
				'state_id' => $request->state_id
			]);

			foreach ($request->translations as $lang => $translation) {
				$statement_category->translation()->create([
					'lang' => $lang,
					'name' => $translation['name']
				]);
			}

			return $statement_category;
		});

		return response()->json([
			'code' => 201,
			'data' => new StatementCategoryResource($data)
		], 200);
	}
	
	public function update(Request $request, StatementCategory $statement_category)
	{
		$ssc = auth()->user()->sscs[0];
		
		$validator = Validator::make($request->all(), [
			'parent_id' => [
				'exists:statement_categories,id',
				Rule::notIn([$statement_category->id])
			],
			'state_id' => 'required|exists:states,id',
			'translations' => 'required',
			'translations.kz.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('kz');
				}),
				'string',
				'max:255',
				Rule::unique('statement_category_langs', 'name')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'kz')->whereIn('id', $ssc->categories()->pluck('id'));
				})->ignore($statement_category->id, 'id')
			],
			'translations.ru.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('ru');
				}),
				'string',
				'max:255',
				Rule::unique('statement_category_langs', 'name')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'ru')->whereIn('id', $ssc->categories()->pluck('id'));
				})->ignore($statement_category->id, 'id')
			],
			'translations.en.name' => [
				Rule::requiredIf(function () use ($request, $ssc){
					return $ssc->hasLang('en');
				}),
				'string',
				'max:255',
				Rule::unique('statement_category_langs', 'name')->where(function ($query) use ($ssc) {
					return $query->where('lang', 'en')->whereIn('id', $ssc->categories()->pluck('id'));
				})->ignore($statement_category->id, 'id')
			],
		]);

		if ($validator->fails()) {
			return response()->json([
				'code' => 422,
				'messages' => $validator->messages() 
			], 200);
		}

		$data = DB::transaction(function() use ($request, $statement_category, $ssc) {
			$statement_category->update([
				'parent_id' => $request->parent_id,
				'state_id' => $request->state_id
			]);

			foreach ($request->translations as $lang => $translation) {
				if ($statement_category->translations()->where('lang', $lang)->first()) {
					$statement_category->translations()->where('lang', $lang)->update([
						'name' => $translation['name']
					]);
				}
				
				if (!$statement_category->translations()->where('lang', $lang)->first()) {
					$statement_category->translations()->create([
						'lang' => $lang,
						'name' => $translation['name']
					]);
				}
			}

			return $statement_category;
		});

		return response()->json([
			'code' => 201,
			'data' => new StatementCategoryResource($data)
		], 200);
	}
	
	public function destroy(StatementCategory $statement_category)
	{
		$statement_category->delete();
		
		return response()->json([
			'code' => 204
		], 200);
	}
}
