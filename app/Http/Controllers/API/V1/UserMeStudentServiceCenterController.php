<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use App\Http\Resources\StudentServiceCenterResource;

use App\Models\StudentServiceCenterLang;
use App\Models\StudentServiceCenter;

use Validator;
use DB;

class UserMeStudentServiceCenterController extends Controller
{
    public function index(Request $request)
    {
		if (auth()->user()->currentRole()->slug == 'student') {
			$ssc = auth()->user()->student->card->group->speciality->department->faculty->university->studentServiceCenter;
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-head') {
			$ssc = auth()->user()->sscs[0];
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-administrator') {
			$ssc = auth()->user()->sscs[0];
		}
		
		if (auth()->user()->currentRole()->slug == 'ssc-employee') {
			$ssc = auth()->user()->sscs[0];
		}

        return new StudentServiceCenterResource($ssc);
    }
	
	public function update(Request $request)
	{
		$ssc = StudentServiceCenter::query()
			->whereHas('users', function($query) {
				$query->where('user_id', auth()->user()->id);
			})
			->first();
		
		$validator = Validator::make($request->all(), [
			'translations' => 'required|array|size:'.count($ssc->langs),
			'translations.kz.name' => 'nullable|string|max:255',
			'translations.ru.name' => 'nullable|string|max:255',
			'translations.en.name' => 'nullable|string|max:255',
			'operating_modes' => 'required|array',
			'operating_modes.*.time_start' => 'required',
			'operating_modes.*.time_end' => 'required',
			'accepting_applications_24_7' => 'required|in:0,1'
		]);

		if ($validator->fails()) {
			return response()->json([
				'code' => 422,
				'messages' => $validator->messages() 
			], 200);
		}
		
		$data = DB::transaction(function() use ($request) {
			$ssc = StudentServiceCenter::query()
				->whereHas('users', function($query) {
					$query->where('user_id', auth()->user()->id);
				})
				->first();

			$ssc->update([
				'accepting_applications_24_7' => strval($request->accepting_applications_24_7)
			]);
			
			foreach ($request->translations as $lang => $translation) {
				$ssc->translations()->where('lang', $lang)->update([
					'name' => $translation['name']
				]);
			}
			
			foreach ($request->operating_modes as $operating_mode_key => $operating_mode_value) {
				$ssc->operating_modes()->where('week_id', $operating_mode_key)->update([
					'time_start' => $operating_mode_value['time_start'],
					'time_end' => $operating_mode_value['time_end']
				]);
			}
		});
		
		return response()->json([
			'code' => 200
		], 200);
	}
}
