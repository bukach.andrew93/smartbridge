<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use App\Models\User;
use App\Models\See;

use Validator;

class UserSeeController extends Controller
{
    public function store(Request $request, User $user)
    {
    	$validator = Validator::make($request->all(), [
            'type_id' => 'required|integer|exists:see_types,id',
	        'series' => 'required|string|max:255',
	        'number' => 'required|string|max:255',
	        'individual_code_of_the_tested' => 'required|string|max:255',
	        'date_of_issue' => 'required|date_format:Y-m-d',
	        'src' => 'required|file|mimes:pdf|max:5120'
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages()
            ], 422);
        }

        $user->sees()->create([
        	'type_id' => $request->type_id,
	        'series' => $request->series,
	        'number' => $request->number,
	        'individual_code_of_the_tested' => $request->individual_code_of_the_tested,
	        'date_of_issue' => $request->date_of_issue,
	        'src' => Storage::put('uploads/'.date('Y/m/d/h/i/s'), $request->src)
        ]);

        return response()->json(null, 201);
    }
}
