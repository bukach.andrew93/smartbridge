<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\NationalityResource;

use App\Models\Nationality;

use Validator;

class NationalityController extends Controller
{
    public function index(Request $request)
    {
    	$nationalities = Nationality::query();

    	$nationalities = $nationalities->paginate($request->has('per_page') ? $request->per_page : 15);

    	return NationalityResource::collection($nationalities);
    }
}
