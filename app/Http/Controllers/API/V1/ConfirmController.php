<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;

use App\Models\UserEmailConfirm;

use Validator;
use DB;

class ConfirmController extends Controller
{
	public function confirm(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'hash' => [
                'required',
                Rule::exists('user_email_confirms')->where(function ($query) use ($request) {
                    $query->where('fit', '1')->where('hash', $request->hash);
                })
            ]
        ]);

        if ($validator->fails()) {
            return response()->json([
				'code' => 422,
                'messages' => $validator->messages() 
            ], 200);
        }

        $result = DB::transaction(function() use ($request) {
            $email_confirm = UserEmailConfirm::query()
                ->where('hash', $request->hash)
                ->first();
			
			$email_confirm->user()->update([
				'email_verified' => '1',
                'access' => '1'
			]);
			
			$email_confirm->update([
                'fit' => '0',
                'verified_at' => date('Y-m-d H:i:s')
            ]);
        });

        return response()->json([
			'code' => 200
		], 200);
    }
	
    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:8|max:255|confirmed',
            'hash' => [
                'required',
                Rule::exists('user_email_restores')->where(function ($query) use ($request) {
                    $query->where('fit', 1)->where('hash', $request->hash);
                })
            ]
        ]);

        if ($validator->fails()) {
            return response()->json([
                'messages' => $validator->messages() 
            ], 422);
        }

        $result = DB::transaction(function() use ($request) {
            $email_restore = UserEmailRestore::query()
                ->where('hash', $request->hash)
                ->first();

            $email_restore->user()->update([
                'password' => bcrypt($request->password)
            ]);

            $email_restore->update([
                'fit' => 0,
                'verified_at' => date('Y-m-d H:i:s')
            ]);
        });

        return response()->json([], 200);
    }
}
