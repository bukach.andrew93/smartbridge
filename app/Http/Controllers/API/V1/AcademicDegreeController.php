<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Resources\AcademicDegreeResource;

use App\Models\AcademicDegreeLang;
use App\Models\AcademicDegree;

use Validator;
use DB;

class AcademicDegreeController extends Controller
{
    public function index(Request $request)
    {
    	$academic_degrees = AcademicDegree::query();

        if ($request->has('filter.university_id') && $request->filter['university_id']) {
            $academic_degrees->where('university_id', $request->filter['university_id']);
        }

        $academic_degrees = $academic_degrees->paginate();

        return AcademicDegreeResource::collection($academic_degrees);
    }

    public function store(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'university_id' => [
                /*
                Rule::requiredIf(function () use ($request){
                    return collect($request->langs)->contains('kz');

                    if () {
                        # code...
                    }
                }),
                */
                'nullable',
                'exists:universities,id'
            ],
            'state_id' => [
                'required',
                'exists:states,id'
            ],
            'translations' => [
                'required',
                'array'
            ],
            'translations.kz' => [
                'required',
                'array'
            ],
            'translations.kz.name' => [
                'required',
                'string',
                'max:255'
            ],
            'translations.ru' => [
                'required',
                'array'
            ],
            'translations.ru.name' => [
                'required',
                'string',
                'max:255'
            ],
            'translations.en' => [
                'required',
                'array'
            ],
            'translations.en.name' => [
                'required',
                'string',
                'max:255'
            ]
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages()
            ], 422);
        }

        $data = DB::transaction(function() use ($request) {
            if (auth()->user()->currentRole()->slug == 'content-manager') {
                $university = auth()->user()->content_manager->card->university;
            }

	        $academic_degree = AcademicDegree::create([
	        	'university_id' => $university->id,
                'state_id' => $request->state_id
	        ]);

            if ($request->has('translations')) {
                foreach ($request->translations as $key => $value) {
                    $academic_degree->translations()->create([
                        'lang' => $key,
                        'name' => $value['name']
                    ]);
                }
            }

            return [
                'academic_degree' => $academic_degree
            ];
	    });

        return response()->json([
            'code' => 201,
            'data' => $data['academic_degree']
        ], 200);
    }

    public function show(AcademicDegree $academic_degree)
    {
    	return new AcademicDegreeResource($academic_degree);
    }

    public function update(Request $request, AcademicDegree $academic_degree)
    {
        $validator = Validator::make($request->all(), [
            'state_id' => [
                'required',
                'integer',
                'exists:states,id'
            ],
            'translations' => [
                'required',
                'array'
            ],
            'translations.kz' => [
                'required',
                'array'
            ],
            'translations.kz.name' => [
                'required',
                'string',
                'max:255'
            ],
            'translations.ru' => [
                'required',
                'array'
            ],
            'translations.ru.name' => [
                'required',
                'string',
                'max:255'
            ],
            'translations.en' => [
                'required',
                'array'
            ],
            'translations.en.name' => [
                'required',
                'string',
                'max:255'
            ]
        ]);

        if ($validator->fails()) {
            return response()->json([
            	'messages' => $validator->messages()
            ], 422);
        }

        DB::transaction(function() use ($request, $academic_degree) {
            $academic_degree->update([
	        	'state_id' => $request->state_id
	        ]);

            if ($request->has('translations')) {
                foreach ($request->translations as $key => $value) {
                	$academic_degree->translations()->where('lang', $key)->update([
                        'name' => $value['name']
                    ]);
                }
            }
	    });
        
    	return response()->json([
            'code' => 200
        ], 200);
    }

    public function destroy(AcademicDegree $academic_degree)
    {
        $academic_degree->delete();

        return response()->json([
            'code' => 204
        ], 200);
    }
}
