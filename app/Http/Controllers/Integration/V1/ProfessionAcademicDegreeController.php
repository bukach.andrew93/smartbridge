<?php

namespace App\Http\Controllers\Integration\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\ProfessionAcademicDegree;
use App\Models\AcademicDegree;
use App\Models\Integration;
use App\Models\Profession;

use Validator;
use DB;

class ProfessionAcademicDegreeController extends Controller
{
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'university_id' => 'required|integer|exists:universities,id',
			'batch' => 'required|string|max:255',
			'data' => 'required|array|max:500',
			'data.*' => 'required|array',
			'data.*.profession_id' => 'required|integer',
			'data.*.academic_degree_id' => 'required|integer',
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		$time_start = microtime(true);

		$actions = [
			'created' => 0,
			'updated' => 0,
			'deleted' => 0
		];

		$langs = [
			'kz',
			'ru',
			'en'
		];

		if (count($request->data) > 0) {
			foreach ($request->data as $data) {
				$data = DB::transaction(function() use ($request, $data, $langs) {
					$created = 0;
					$updated = 0;
					$deleted = 0;

					$profession = Profession::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['profession_id']
						]))
						->first();

					$academic_degree = AcademicDegree::query()
	                	->where('platonus_key', implode('-', [
							$request->university_id,
							$data['academic_degree_id']
						]))
	                    ->first();

	                $profession_academic_degree = ProfessionAcademicDegree::query()
	                	->where('profession_id', $profession->id)
	                    ->where('academic_degree_id', $academic_degree->id)
	                    ->first();

					if ($profession_academic_degree) {
						
					} else {
						ProfessionAcademicDegree::create([
							'profession_id' => $profession->id,
							'academic_degree_id' => $academic_degree->id,
							'state_id' => 2
						]);

						$created = 1;
					}

					return [
						'created' => $created,
						'updated' => $updated,
						'deleted' => $deleted,
					];
				});

				$actions['created'] = $actions['created'] + $data['created'];
				$actions['updated'] = $actions['updated'] + $data['updated'];
				$actions['deleted'] = $actions['deleted'] + $data['deleted'];
			}
		}

		$integration = Integration::query()
			->where('batch', $request->batch)
			->first();

		if ($integration) {
			$integration->update([
				'created' => $integration->created + $actions['created'],
				'updated' => $integration->updated + $actions['updated'],
				'deleted' => $integration->deleted + $actions['deleted'],
				'time' => round($integration->time + round(microtime(true) - $time_start, 4), 4)
			]);
		} else {
			Integration::create([
				'university_id' => $request->university_id,
				'system' => 'platonus',
				'table' => 'professions_academic_degrees',
				'batch' => $request->batch,
				'created' => $actions['created'],
				'updated' => $actions['updated'],
				'deleted' => $actions['deleted'],
				'time' => round(microtime(true) - $time_start, 4)
			]);
		}

		return response()->json([
			'code' => 201,
			'time' => round(microtime(true) - $time_start, 4)
		], 200);
	}
}
