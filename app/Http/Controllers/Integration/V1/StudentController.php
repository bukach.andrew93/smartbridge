<?php

namespace App\Http\Controllers\Integration\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\NotificationViewUserRole;
use App\Models\DormitoryNeedStatus;
use App\Models\StudentCardStatus;
use App\Models\NotificationView;
use App\Models\AcademicCalendar;
use App\Models\StudyLanguage;
use App\Models\Integration;
use App\Models\AccountView;
use App\Models\StudentCard;
use App\Models\PaymentForm;
use App\Models\Curriculum;
use App\Models\Speciality;
use App\Models\StudyForm;
use App\Models\GrantType;
use App\Models\Benefit;
use App\Models\Country;
use App\Models\Quota;
use App\Models\Group;
use App\Models\User;
use App\Models\Cato;
use App\Models\Role;

use Validator;
use DB;

class StudentController extends Controller
{
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'university_id' => 'required|integer|exists:universities,id',
			'batch' => 'required|string|max:255',
			'data' => 'required|array|max:500',
			'data.*' => 'required|array',
			'data.*.id' => 'required|integer',
			'data.*.surname' => 'required|string|max:255',
			'data.*.name' => 'required|string|max:255',
			'data.*.birth_date' => 'required|date_format:Y-m-d',
			'data.*.quota_id' => 'nullable|integer',
			'data.*.benefit_id' => 'nullable|integer',
			'data.*.academic_calendar_id' => 'required|integer',
			'data.*.dormitory_need_status_id' => 'nullable|integer',
			'data.*.transcript_number' => 'nullable|integer',
			'data.*.date_start' => 'required|date_format:Y-m-d',
			'data.*.gpa' => 'required',
			'data.*.course_number' => 'required|integer',
			'data.*.arrived_country_id' => 'required|integer',
			'data.*.arrived_cato_id' => 'nullable|integer',
			'data.*.grant_number' => 'nullable|string|max:255',
			'data.*.grant_type_id' => 'nullable|integer',
			'data.*.payment_form_id' => 'required|integer',
			'data.*.study_language_id' => 'required|integer',
			'data.*.group_id' => 'required|integer',
			'data.*.speciality_id' => 'required|integer',
			'data.*.arrived_cato_name' => 'nullable|string|max:255'
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		$time_start = microtime(true);

		$actions = [
			'created' => 0,
			'updated' => 0,
			'deleted' => 0
		];

		$langs = [
			'kz',
			'ru',
			'en'
		];

		if (count($request->data) > 0) {
			foreach ($request->data as $data) {
				$data = DB::transaction(function() use ($request, $data, $langs) {
					$created = 0;
					$updated = 0;
					$deleted = 0;

					$arrived_country = isset($data['arrived_country_id']) ? Country::query()
						->where('id', $data['arrived_country_id'])
						->first() : null;

					$arrived_cato = isset($data['arrived_cato_id']) ? Cato::query()
						->where('code', $data['arrived_cato_code'])
						->first() : null;

					$curriculum = Curriculum::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['curriculum_id']
						]))
						->first();

					$benefit = isset($data['benefit_id']) ? Benefit::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['benefit_id']
						]))
						->first() : null;

					$quota = isset($data['quota_id']) ? Quota::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['quota_id']
						]))
						->first() : null;

					$academic_calendar = AcademicCalendar::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['academic_calendar_id']
						]))
						->first();

					$dormitory_need_status = isset($data['dormitory_need_status_id']) ? DormitoryNeedStatus::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['dormitory_need_status_id']
						]))
						->first() : null;

					$study_form = StudyForm::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['study_form_id']
						]))
						->first();

					$grant_type = GrantType::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['grant_type_id']
						]))
						->first();

					$group = Group::query()
						->where('integration_fields->platonus->university_id', $request->university_id)
						->where('integration_fields->platonus->id', $data['group_id'])
						->first();

					$speciality = Speciality::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['speciality_id']
						]))
						->first();

					$payment_form = PaymentForm::query()
						->where('platonus_key', implode('-', [
							$request->university_id,
							$data['payment_form_id']
						]))
						->first();

					$study_language = StudyLanguage::query()
						->where('id', $data['study_language_id'])
						->first();

					if (!$study_language) {
						echo $data['id'].'-'.$data['study_language_id'].PHP_EOL;
					}

					$user = User::query()
						->whereHas('student.cards', function($query) use ($request, $data) {
							$query
								->where('integration_fields->platonus->university_id', $request->university_id)
								->where('integration_fields->platonus->id', $data['id']);
						})
						->first();

					$user = !$user ? User::query()
						->where('surname', $data['surname'])
						->where('name', $data['name'])
						->where('birth_date', $data['birth_date'])
						->first() : $user;

					$role = Role::query()
						->where('slug', 'student')
						->first();

					$student = StudentCard::query()
						->where('integration_fields->platonus->university_id', $request->university_id)
						->where('integration_fields->platonus->id', $data['id'])
						->first();

					$student_card_status = StudentCardStatus::query()
						->where('id', 101)
						->first();

					if ($student) {
						$student->update([
							'group_id' => $group->id,
							'speciality_id' => $speciality->id,
							'study_language_id' => $study_language->id,
							'payment_form_id' => $payment_form->id,
							'grant_type_id' => $grant_type ? $grant_type->id : null,
							'grant_number' => isset($data['grant_number']) ? $data['grant_number'] : null,
							'study_form_id' => $study_form->id,
							'course_number' => $data['course_number'],
							'date_start' => $data['date_start'],
							'transcript_number' => isset($data['transcript_number']) ? $data['transcript_number'] : null,
							'dormitory_need_status_id' => $dormitory_need_status ? $dormitory_need_status->id : null,
							'academic_calendar_id' => $academic_calendar->id,
							'curriculum_id' => $curriculum->id,
							'benefit_id' => $benefit ? $benefit->id : null,
							'quota_id' => $quota ? $quota->id : null,
							'status_id' => $student_card_status->id
						]);

						foreach ($langs as $lang) {
							if (isset($data['from_cato_name_'.$lang])) {
								$student->translations()->where('lang', $lang)->update([
									'arrived_cato_name' => $data['from_cato_name_'.$lang]
								]);
							}
						}

						if (!$user->hasRole('student')) {
							$user->roles()->attach($role);
						}

						if (!$user->student) {
							$user->student()->create();
						}

						if (!$user->account) {
							$user->account()->create([
								'view_id' => AccountView::whereNull('university_id')->first()->id,
								'state_id' => 2
							]);
						}

						$views = NotificationView::query()
							->whereIn('slug', [
								'schedule',
								'tiding',
								'event',
								'finance',
								'statement'
							])
							->get();

						if ($views) {
							foreach ($views as $view) {
								$notification_view_user = NotificationViewUserRole::query()
									->where('view_id', $view->id)
									->where('user_id', $user->id)
									->where('role_id', $role->id)
									->first();

								if (!$notification_view_user) {
									NotificationViewUserRole::create([
										'view_id' => $view->id,
										'user_id' => $user->id,
										'role_id' => $role->id,
										'state' => '1'
									]);
								}
							}
						}

						if ($student->wasChanged()) {
							$updated = 1;
						}
					} else {
						$student = StudentCard::create([
							'user_id' => $user->id,
							'group_id' => $group->id,
							'speciality_id' => $speciality->id,
							'study_language_id' => $study_language->id,
							'payment_form_id' => $payment_form->id,
							'grant_type_id' => $grant_type ? $grant_type->id : null,
							'grant_number' => isset($data['grant_number']) ? $data['grant_number'] : null,
							'study_form_id' => $study_form->id,
							'arrived_country_id' => $arrived_country ? $arrived_country->id : null,
							'arrived_cato_id' => $arrived_cato ? $arrived_cato->id : null,
							'course_number' => $data['course_number'],
							'gpa' => $data['gpa'],
							'date_start' => $data['date_start'],
							'transcript_number' => isset($data['transcript_number']) ? $data['transcript_number'] : null,
							'dormitory_need_status_id' => $dormitory_need_status ? $dormitory_need_status->id : null,
							'academic_calendar_id' => $academic_calendar->id,
							'curriculum_id' => $curriculum->id,
							'benefit_id' => $benefit ? $benefit->id : null,
							'quota_id' => $quota ? $quota->id : null,
							'integration_fields' => [
								'platonus' => [
									'university_id' => $request->university_id,
									'id' => $data['id']
								]
							],
							'state_id' => 2,
							'status_id' => $student_card_status->id
						]);

						foreach ($langs as $lang) {
							if (isset($data['from_cato_name_'.$lang])) {
								$student->translation()->create([
									'lang' => $lang,
									'arrived_cato_name' => $data['from_cato_name_'.$lang]
								]);
							}
						}

						if (!$user->hasRole('student')) {
							$user->roles()->attach($role);
						}

						if (!$user->student) {
							$user->student()->create();
						}

						if (!$user->account) {
							$user->account()->create([
								'view_id' => AccountView::whereNull('university_id')->first()->id,
								'state_id' => 2
							]);
						}

						$views = NotificationView::query()
							->whereIn('slug', [
								'schedule',
								'tiding',
								'event',
								'finance',
								'statement'
							])
							->get();

						if ($views) {
							foreach ($views as $view) {
								$notification_view_user = NotificationViewUserRole::query()
									->where('view_id', $view->id)
									->where('user_id', $user->id)
									->where('role_id', $role->id)
									->first();

								if (!$notification_view_user) {
									NotificationViewUserRole::create([
										'view_id' => $view->id,
										'user_id' => $user->id,
										'role_id' => $role->id,
										'state' => '1'
									]);
								}
							}
						}

						$created = 1;
					}

					return [
						'created' => $created,
						'updated' => $updated,
						'deleted' => $deleted,
					];
				});

				$actions['created'] = $actions['created'] + $data['created'];
				$actions['updated'] = $actions['updated'] + $data['updated'];
				$actions['deleted'] = $actions['deleted'] + $data['deleted'];
			}
		}

		$integration = Integration::query()
			->where('batch', $request->batch)
			->first();

		if ($integration) {
			$integration->update([
				'created' => $integration->created + $actions['created'],
				'updated' => $integration->updated + $actions['updated'],
				'deleted' => $integration->deleted + $actions['deleted'],
				'time' => round($integration->time + round(microtime(true) - $time_start, 4), 4)
			]);
		} else {
			Integration::create([
				'university_id' => $request->university_id,
				'system' => 'platonus',
				'table' => 'students',
				'batch' => $request->batch,
				'created' => $actions['created'],
				'updated' => $actions['updated'],
				'deleted' => $actions['deleted'],
				'time' => round(microtime(true) - $time_start, 4)
			]);
		}

		return response()->json([
			'code' => 201,
			'time' => round(microtime(true) - $time_start, 4)
		], 200);
	}
}
