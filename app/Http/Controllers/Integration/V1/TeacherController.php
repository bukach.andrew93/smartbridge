<?php

namespace App\Http\Controllers\Integration\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Integration;
use App\Models\TeacherCard;
use App\Models\User;
use App\Models\Role;

use Validator;
use DB;

class TeacherController extends Controller
{
	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'university_id' => 'required|integer|exists:universities,id',
			'batch' => 'required|string|max:255',
			'data' => 'required|array|max:500',
			'data.*' => 'required|array',
			'data.*.id' => 'required|integer',
			'data.*.surname' => 'required|string|max:255',
			'data.*.name' => 'required|string|max:255',
			'data.*.birth_date' => 'required|date_format:Y-m-d'
		]);

		if ($validator->fails()) {
			return response()->json([
				'messages' => $validator->messages()
			], 422);
		}

		$time_start = microtime(true);

		$actions = [
			'created' => 0,
			'updated' => 0,
			'deleted' => 0
		];

		$langs = [
			'kz',
			'ru',
			'en'
		];

		if (count($request->data) > 0) {
			foreach ($request->data as $data) {
				$data = DB::transaction(function() use ($request, $data, $langs) {
					$created = 0;
					$updated = 0;
					$deleted = 0;

					$user = User::query()
						->where('surname', $data['surname'])
						->where('name', $data['name'])
						->where('birth_date', $data['birth_date'])
						->first();

					$role = Role::query()
						->where('slug', 'teacher')
						->first();

					if ($user && !$user->teacher) {
						$user->teacher()->create();

						$user->roles()->attach($role);

						TeacherCard::create([
							'platonus_key' => implode('-', [
								$request->university_id,
								$data['id']
							]),
							'user_id' => $user->id,
							'date_start' => $data['date_start'],
							'data_end' => isset($data['date_end']) ? $data['date_end'] : null
						]);

						$created = 1;
					}

					$teacher_card = TeacherCard::query()
						->where('platonus_key', sha1(implode('-', [
							$request->university_id,
							$data['id']
						])))
						->first();

					if ($teacher_card) {
						$teacher_card->update([
							'platonus_key' => implode('-', [
								$request->university_id,
								$data['id']
							])
						]);
					}

					return [
						'created' => $created,
						'updated' => $updated,
						'deleted' => $deleted,
					];
				});

				$actions['created'] = $actions['created'] + $data['created'];
				$actions['updated'] = $actions['updated'] + $data['updated'];
				$actions['deleted'] = $actions['deleted'] + $data['deleted'];
			}
		}

		$integration = Integration::query()
			->where('batch', $request->batch)
			->first();

		if ($integration) {
			$integration->update([
				'created' => $integration->created + $actions['created'],
				'updated' => $integration->updated + $actions['updated'],
				'deleted' => $integration->deleted + $actions['deleted'],
				'time' => round($integration->time + round(microtime(true) - $time_start, 4), 4)
			]);
		} else {
			Integration::create([
				'university_id' => $request->university_id,
				'system' => 'platonus',
				'table' => 'teachers',
				'batch' => $request->batch,
				'created' => $actions['created'],
				'updated' => $actions['updated'],
				'deleted' => $actions['deleted'],
				'time' => round(microtime(true) - $time_start, 4)
			]);
		}

		return response()->json([
			'code' => 201,
			'time' => round(microtime(true) - $time_start, 4)
		], 200);
	}
}
